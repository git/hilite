/*
 * hilite.c
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.c"

void help() {
  printf("Usage: hilite [-build|-validate][SRC][DEST]\n");
}
int main(int argc,char **argv) {
  enter();
  int build=0,validate=0,i;
  if ((i=argvFind(argc,argv,"-build"))!=-1) argvRemove(&argc,argv,i),build=1;
  if ((i=argvFind(argc,argv,"-validate"))!=-1) argvRemove(&argc,argv,i),validate=1;
  if (build==0 && validate==0) build=1;
  if (build==1 && validate==1) error("Options -build and -validate can not occur together");
  int res=0;
  if (argc>1 && (!strcmp(argv[1],"-h") || *argv[1]=='-')) help();
  else {
    argvCut(argc,argv,0);
    char *jsfile=validate?"hilite_srv.js -validate":"hilite_srv.js -build",
         *cmd=nodeCmd(argc,argv,jsfile);
  //printf("<%s>\n",cmd);
    res=system(cmd);
  }
  leave(res,0);
  return 0; // Not reached
}
