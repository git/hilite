/*
 * defs.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Important definitions
var hilite_ready=false;
function hilite_init() { // The various characters and their categories
// TODO: put the appropriate splitters depending on each mode, e.g. right now, I'm not sure &nbsp; is recognized correctly in HTML
// FIXME: hilite_init() should become tokenize_init(), or something like that.
  if (!hilite_ready) {
    for (i=0;i<=255;i++) charnat[i]=UNKNOWN;
    for (i=asc("A");i<=asc("Z");i++) charnat[i]=ALPHANUM;
    for (i=asc("a");i<=asc("z");i++) charnat[i]=ALPHANUM;
    for (i=asc("0");i<=asc("9");i++) charnat[i]=ALPHANUM;
    charnat[228/*ae*/]=ALPHANUM; // TODO: add all the other accented characters (e.g. french, norse, etc.)
    charnat[246/*oe*/]=ALPHANUM;
    charnat[252/*ue*/]=ALPHANUM;
    charnat[asc("$")]=ALPHANUM;
    charnat[asc("?")]=ALPHANUM;
    charnat[asc("_")]=ALPHANUM;
    charnat[asc("@")]=ALPHANUM;
    charnat[asc("!")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    charnat[asc("-")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    //charnat[asc("&")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    charnat[asc("#")]=SERVER?OPCHAR:ALPHANUM;
    charnat[asc("&")]=SPLITTER; // FIXME: Inside <code>...</code>, etc. (i.e., all the things that contain code (php code, javascript code, etc.) when used not inside <script>...</script>), like e.g., "=>" are turned to e.g. "&amp;&gt;"
    charnat[asc("+")]=SPLITTER;
    charnat[asc("*")]=SPLITTER;
    charnat[asc("%")]=SPLITTER;
    charnat[asc(",")]=SPLITTER;
    charnat[asc(":")]=SPLITTER;
    charnat[asc(";")]=SPLITTER;
    charnat[asc(".")]=SPLITTER;
    charnat[asc("=")]=SPLITTER;
    charnat[asc("|")]=SPLITTER;
    charnat[asc("(")]=charnat[asc(")")]=SPLITTER;
    charnat[asc("[")]=charnat[asc("]")]=SPLITTER;
    charnat[asc("{")]=charnat[asc("}")]=SPLITTER;    
    charnat[asc("<")]=charnat[asc(">")]=charnat[asc("/")]=SPLITTER;
    charnat[asc("^")]=SPLITTER;
    charnat[asc("\\")]=ALPHANUM;
    charnat[asc("\t")]=BLANK;
    charnat[asc("\n")]=BLANK;
    charnat[asc("\r")]=BLANK;
    charnat[asc(" ")]=BLANK;
    charnat[asc("'")]=QUOTE;
    charnat[asc("\"")]=DQUOTE;
  }
  hilite_ready=true;
}
