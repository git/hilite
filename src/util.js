/*
 * util.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Positions in files
function filePos(fname,li,col) {
  return {"fname":fname,"li":li,"col":col};
}
function filePosDup(fpos) {
  return filePos(fpos.fname,fpos.li,fpos.col);
}
function filePosLiCol(fpos,li,col) {
  fpos.li=li;
  fpos.col=col;
}
function filePosSerialize(fpos) {
  var res="";
  if (fpos.fname!=null) res+=fpos.fname+", ";
  res+="line "+fpos.li+", col "+fpos.col;
  return res;
}

// Error (filePos)
var _errorFilePos=null;
function errorPos() {
  if (_errorFilePos==null) return null;
  return filePosDup(_errorFilePos);
}
function errorFileName() {
  if (_errorFilePos==null) return null;
  return _errorFilePos.fname;
}
function errorLi() {
  if (_errorFilePos==null) return 0;
  return _errorFilePos.li;
}
function errorCol() {
  if (_errorFilePos==null) return 0;
  return _errorFilePos.col;
}
function errorSetPos(fname,li,col) {
  _errorFilePos=filePos(fname,li,col);
}
function errorSetLiCol(li,col) {
  if (_errorFilePos==null) return;
  filePosLiCol(_errorFilePos,li,col);
}
function errorResetPos() {
  _errorFilePos=null;
}

// Error
var SERVER=false;
function stop(errno) {
  if (SERVER) process.exit(errno); else zboc(1); // To stop it
}
function error(msg) {
  if (_errorFilePos!=null) msg+=" --> "+filePosSerialize(_errorFilePos);
  (SERVER?console.log:alert)(msg);
  stop(1);
}

// Useful constants
function ftrue() { return true; }
function ffalse() { return false; }
function ftrue1(a,b) { return true; }
function ffalse1(a,b) { return false; }
function ftrue2(a,b) { return true; }
function ffalse2(a,b) { return false; }

// Objects
function _proto(X) {
  return X.constructor.prototype;
}

function isNull(o) { return o==null; }
function isBoolean(o) { return o!=null && _proto(o)==_proto(true); }
function isNumber(o) { return o!=null && _proto(o)==_proto(1); }
function isString(o) { return o!=null && _proto(o)==_proto("A"); }
function isArray(o) { return o!=null && _proto(o)==_proto([]); }
function isDictionary(o) { return o!=null && _proto(o)==_proto({}); }
function isFunction(X) { return _proto(X)==_proto(function() {}); }

// Console & displays
function out(s) {
  document.write(s);
}
function br(s) {
  out("<br>");
}
function hr(s) {
  out("<hr>");
}

function stringEscape(s) {
  var res="";
  for (var i=0;i<s.length;i++) {
    var c=s[i];
    if (c=='"') c="\\\"";
    if (c=='\n') c="\\n";
    res+=c;
  }
  return res;
}
var displaying=[],display_mode="cooked";
function displayMode(mode) {
  display_mode=mode;
}
function display(o) { // Similar to JSON.stringify()
  var res=null;
  if (isNull(o)) res="null";
  if (isBoolean(o)) res=o.toString();
  if (isNumber(o)) res=o.toString();
  if (isString(o)) {
  /*if (display_mode=="raw")*/ res=o;
    if (display_mode=="cooked") res='"'+o+'"';
    if (display_mode=="html") res=htmlEscapeChars(stringEscape(o));
  }
  if (contains(displaying,o)) res="^^";
  else
  if (isArray(o)) {
    displaying.push(o);
    res="[";
    for (var i=0;i<o.length;i++) {
      if (i>0) res+="|";
      res+=display(o[i]);
    }
    res+="]";
    displaying.pop();
  }
  else
  if (isDictionary(o)) {
    displaying.push(o);
    res="{";
    var first=true;
    for (var val in o) {
      if (!first) res+="|"; else first=false;
      if (val!="parent") {
        res+=val+"="+display(o[val]);
      }
    }
    res+="}";
    displaying.pop();
  }
  return res;
}

// Characters
function asc(ch) {
  return ch.charCodeAt(0)&255;
}
function chr(i) {
  return String.fromCharCode(i);
}

// Strings
function trim(s,chars,left,right) {
  if (chars==undefined) chars=" ";
  if (left==undefined) left=true;
  if (right==undefined) right=true;
  var res="",a=s.split(""),i;
  if (left) {
    i=0;
    while (i<a.length && chars.indexOf(a[i])!=-1) {
      a[i]=null;
      i++;
    }
  }
  if (right) {
    i=a.length-1;
    while (i>=0 && chars.indexOf(a[i])!=-1) {
      a[i]=null;
      i--;
    }
  }
  for (i=0;i<a.length;i++) if (a[i]!=null) res+=a[i];
  return res;
}
function startsWith(s,pref) {
  if (s.length<pref.length) return false;
  else {
    return s.substring(0,pref.length)==pref;
  }
}
function endsWith(s,suff) {
  if (s.length<suff.length) return false;
  else {
    return s.substring(s.length-suff.length,s.length)==suff;
  }
}
function stringFind(s,ss) {
  for (var i=0;i<s.length;i++) {
    if (startsWith(s.substring(i,s.length),ss)) return i;
  }
  return -1;
}
function splitTrim(s,chars) {
  var a=s.split(chars);
  for (var i=0;i<a.length;i++) a[i]=trim(a[i]," ",true,true);
  return a;
}
function replaceAll(s,s1,s2) {
  var s0;
  do {
    s0=s;
    s=s.replace(s1,s2);
  }
  while (s!=s0);
  return s;
}
function lcase(s) {
  return s.toLowerCase();
}
function ucase(s) {
  return s.toUpperCase();
}

// Arrays
function length(o) {
  if (isString(o) || isArray(o)) return o.length;
  if (isDictionary(o)) return Object.getOwnPropertyNames(o).length;
  return undefined;
}
function empty(st) {
  return length(st)==0;
}
function last(st) { // TODO: Shit, top(), is already defined. Find a way to have it, or another decent name
  if (empty(st)) return null;
  return st[st.length-1];
}
function contains(a,o) {
  if (isString(a)) return stringFind(a,o)!=-1;
  else
  if (isArray(a)) {
    for (var i=0;i<a.length;i++) if (a[i]===o) return true;
  }
  else error("contains");
  return false;
}
function index(a,o) {
  for (var i=0;i<a.length;i++) if (a[i]===o) return i;
  return -1;
}
function arrayToDict(a) {
  var d={};
  for (var i=0;i<a.length;i++) if (a[i]!=null) d[a[i][0]]=a[i][1];
  return d;
}
function splice(t,i,ndel,t2) {
  t.splice.apply(t,[i,ndel].concat(t2));
}
function arrayCopy(a,i0,j0) {
  var res=[];
  for (var i=i0;i<j0;i++) res.push(a[i]);
  return res;
}
function arrayTrim(a,left,right) {
  var i0=0,j0=length(a);
  if (left)
    while (i0<length(a) && isString(a[i0]) && length(a[i0])>0 && charIs(a[i0][0],BLANK)) i0++;
  if (right)
    while (j0>0 && isString(a[j0-1]) && length(a[j0-1])>0 && charIs(a[j0-1][0],BLANK)) j0--;
  return arrayCopy(a,i0,j0);
}

// Argv
function argvRemove(argv,j) {
  splice(argv,j,1,[]);
}
