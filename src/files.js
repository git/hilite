/*
 * files.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Files
function fileExists(fname) {
  return fs.existsSync(fname);
}
function fileRead0(fname) {
  if (!fileExists(fname)) error("File not found: "+fname);
  return fs.readFileSync(fname,"ascii");
}
var htmlFileRead_CRSplitter=false;
function htmlFileRead(fname) {
  val=fileRead0(fname);
  val=replaceAll(val,"\r\n","\n");
  hilite_init();
  if (htmlFileRead_CRSplitter) charnat[asc("\n")]=SPLITTER; // FIXME: Crap hack for splitting blanks containing CRs in some cases
  val=trim(val,"\n",1,0);
  val=trim(val," \n",0,1);
  bufstart(val);
  val=htmlParse(filePos(fname,1,1));
  return val;
}
_READ={ "html":htmlFileRead };
function fileReadSet(ext,readFunc) {
  _READ[ext]=readFunc;
}
function fileExt(fname) {
  var a=fname.split(".");
  return a[length(a)-1];
}
function fileRead(fname) {
  var dir=path.dirname(fname)+"/";
  fname=path.basename(fname);
  var val="";
  var readFunc=_READ[fileExt(fname)];
  if (readFunc!=undefined) {
    val=readFunc(dir+fname);
  }
  else {
    val=fs.readFileSync(dir+fname,"ascii");
    val=replaceAll(val,"\r","");
    val=trim(val,"\n",1,0);
    val=trim(val," \n",0,1);
  }
  return val;
}
function fileWrite(fname,str) {
  try {
    fs.writeFileSync(fname,str);
  }
  catch (err) {
    error("Can't write file: "+fname);
  }
}
function fileDelete(fname) {
  fs.unlinkSync(fname);
}

// Directories
function dirCreate(fname) {
  try {
    fs.mkdirSync(fname);
  }
  catch (err) {
    error("Can't create directory: "+fname);
  }
}
function isDir(fname) {
  var fd=fs.openSync(fname,"r");
  var stats=fs.fstatSync(fd);
  return stats.isDirectory();
}
function vfileCreate(fname,isDir,parent,val) {
//console.log("Loading "+fname);
  return {"fname":fname,"isDir":isDir,isModified:false,"val":val, "parent":parent};
}
function dirRead(fname,mask) {
  var predir=path.dirname(fname)+"/";
  fname=path.basename(fname);
  var d=vfileCreate(fname,true,null,{});
  var dir=fname;
  if (predir!="") dir=predir+"/"+fname;
  var a=fs.readdirSync(dir);
  for (var i=0;i<a.length;i++) {
    var b=isDir(dir+"/"+a[i]);
    if (b) {
      d.val[a[i]]=dirRead(dir+"/"+a[i],mask);
      d.val[a[i]].parent=d;
    }
    else {
      d.val[a[i]]=vfileCreate(a[i],b,d,null);
      if (mask==undefined || endsWith(a[i],mask)) d.val[a[i]].val=fileRead(dir+"/"+a[i]);
    }
  }
  return d;
}
function foreach_vfile(d,func) {
  for (var fname in d.val) {
    var f=d.val[fname];
    if (f.isDir) foreach_vfile(f,func);
            else func(f);
  }
  func(d);
}
function vfilePathname(vf) {
  var a=[];
  do {
    a.push(vf.fname);
    vf=vf.parent;
  }
  while (vf!=null);
  return replaceAll(path.normalize(a.reverse().join("/")),"\\","/");
}
function isChildPath(p,parent) {
  p=path.normalize(p);
  parent=path.normalize(parent);
  if (parent[0]=="." && p[0]!='/') p="./"+p; //FIXME: Sort out all the other cases with "."
  return startsWith(p,parent);
}
