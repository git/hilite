/*
 * transforms.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Rules
function ruleCreate(tag,test,func) {
  var res={ "tag":null, "test":null, "func":null,
            "frule":null };
  res.tag=tag;
  res.test=test;
  res.func=func;
  return res;
}
function frulesCreate(tr) {
  return { "rules":[], "transform":tr };
}

function ruleApply(r,t) {
  return r.func(t,t);
}

// Transforms
function transformCreate() {
  return { "tag":{}, "string":{}, "begblocks":[] };
}
function frulesAddRule(f,r) {
  f.rules.push(r);
  r.frules=f;
}
function transformAddRule(t,r) {
  if (isNull(t.tag[""])) t.tag[""]=frulesCreate(t);
  frulesAddRule(t.tag[""],ruleCreate("",ftrue2,r));
}

function transformApplyFlat(t,node) {
  var first=true;
  for (n in t.tag) {
    for (var i=0;i<t.tag[n].rules.length;i++) {
      node=ruleApply(t.tag[n].rules[i],node);
      if (_foreach_cut) break;
    }
    if (_foreach_cut) break;
  }
  return node;
}
function transformApply(t,node) {
  node=foreach_elt(node,function (elt) {
    return transformApplyFlat(t,elt);
  });
  return node;
}
function transform(node,t) {
  return transformApply(t,node);
}
