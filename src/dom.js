/*
 * dom.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Expanding highlighted tags (DOM)
function dom_replace(tag,node,markup) {
  //if (cdata[tag]) {
    var div=document.createElement("div");
    div.innerHTML=markup;
    node.parentNode.replaceChild(div,node);
  /*}
    else {
      node.innerHTML=markup;
  }*/
}
function dom_transform(tag,transform) {
  var todo0=document.getElementsByTagName(tag),val=null;
  var todo=[];
  var i;
  for (i=0;i<todo0.length;i++) todo.push(todo0[i]);
  for (i=0;i<todo.length;i++) {
    var attrs={};
    for (var attr,j=0,a=todo[i].attributes,n=a.length;j<n;j++) {
      attr=a[j];
      attrs[attr.nodeName]=attr.value;
    }
    var text=transform(attrs,todo[i].innerHTML);
    if (text!=null) {
      dom_replace(tag,todo[i],text);
    }
  }
}