/*
 * parser.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Trees
function tagCreate(state,tag,elts,li,col) {
  if (li==undefined) li=-1;
  if (col==undefined) col=-1;
  return {"state":state,"parent":null,"tag":tag,"attrs":{},"elts":elts,"licol":[li,col]};
}
function isXmlPI(tag) {
  return isDictionary(tag) && tag.state=="?" && !isHtmlComment(tag);
}
function isHtmlComment(tag) {
  return isDictionary(tag) && tag.state=="?" && tag.tag=="!--";
}
function isOpeningTag(tag) {
  return isDictionary(tag) && tag.state=="<";
}
function isClosingTag(tag) {
  return isDictionary(tag) && tag.state==">";
}
function isCDataTag(tag) {
  return isDictionary(tag) && cdata[tag.tag];
}
function isOptCloseTag(tag) {
  return isDictionary(tag) && optclose[tag.tag];
}
function isSelfClosingTag(tag) {
  return isDictionary(tag) && tag.state=="</";
}
function isSelfTag(tag) { // FIXME: Messy ...
  return !isClosingTag(tag) && ((isDictionary(tag) && selftag[tag.tag]) || isCDataTag(tag) || isSelfClosingTag(tag));
}
function isClosedTag(tag) {
  return isDictionary(tag) && tag.state=="<>";
}
function isTag(tag,name,attr,fval) {
  if (!isDictionary(tag) || !(tag.state=="<>" || tag.state=="<") || tag.tag!=name) return false;
  return fval(attrVal(tag.attrs[attr]));
}
function tagAdd(tag,elt) {
  tag.elts.push(elt);
  if (isDictionary(elt)) elt.parent=tag;
}
function tagReplace(dest,src) {
  if (!isDictionary(dest) || !isDictionary(src)) error("tagReplace");
  dest.state=src.state;
  dest.tag=src.tag;
  dest.attrs=src.attrs;
  dest.elts=src.elts;
}
function treeCopy(p,t) {
  var res=t;
  if (isString(t)) ;
  else
  if (isDictionary(t)) {
    res={"state":t.state,"parent":p,"tag":t.tag,"attrs":{},"elts":[]};
    for (var n in t.attrs) res.attrs[n]=t.attrs[n];
    for (var i=0;i<t.elts.length;i++) res.elts.push(treeCopy(res,t.elts[i]));
  }
  else error("treeCopy");
  return res;
}

// Traversals
var _foreach_elt_modified;
function foreach_elt_modified() {
  return _foreach_elt_modified;
}
var _foreach_cut=false;
function foreach_cut() {
  _foreach_cut=true;
}
function foreach_elt_bis(node,func) {
  if (isDictionary(node)) {
    if (node.tag!="!--") {
      var onode=node;
      node=func(node);
      if (node!=onode) _foreach_elt_modified=true;
      if (!_foreach_cut) {
        var elt=node.elts;
        for (var i=0;i<elt.length;i++) {
          elt[i]=foreach_elt_bis(elt[i],func);
        }
      }
    }
    _foreach_cut=false;
  }
  return node;
}
function foreach_elt(node,func) {
  _foreach_elt_modified=false;
  _foreach_cut=false;
  return foreach_elt_bis(node,func);
}
function foreach_text_bis(node,attrs,func) {
  var res=node;
  if (isDictionary(node)) {
    var elt=node.elts;
    for (var i=0;i<elt.length;i++) {
      elt[i]=foreach_text_bis(elt[i],node,func);
      if (_foreach_cut) break;
    }
    _foreach_cut=false;
  }
  else
  if (isString(node)) res=func(node,attrs);
  return res;
}
function foreach_text(node,func) {
  _foreach_cut=false;
  return foreach_text_bis(node,node,func);
}

// Parsing & serializing HTML
function cdataNext(end,end2) { // FIXME: Skip comments and strings, in case they would contain end
  var res=null,ptr0=ptr,s,s2;
  var first=true;
  do {
    findChar(end[0]);
    s=s2="";
    if (!bufEof() && (s=buf.substring(ptr,ptr+end.length))!=end
                  && (s2=buf.substring(ptr,ptr+end2.length))!=end2) ptrNext();
  }
  while (!bufEof() && s!=end && s2!=end2);
  if (!bufEof()) res=buf.substring(ptr0,ptr);
  return res;
}
function tagReadAttrs(tag) {
  str=next(true,false);
  while (str!="/" && str!=">") {
    var name=str;
    var li=errorLi(),col=errorCol();
    str=next(true,false);
    if (str!="=") errorSetLiCol(li,col),error("Attribute expected");
    str=next(true,false);
    tag.attrs[name]=str;
    str=next(true,false);
  }
  if (str=="/") {
    tag.state="</";
    var li=errorLi(),col=errorCol();
    str=next(true,false);
    if (str!=">") errorSetLiCol(li,col),error("'>' expected [<tag ... />]");
  }
}
function htmlNext() {
  if (ptr>=buf.length) return null;
  var li=errorLi(),col=errorCol(),
      str=next(false,true);
  if (str!="<") res=str;
  else 
  tag: {
    var opn=charnat[asc("+")]; // FIXME: Hack
    charnat[asc("+")]=ALPHANUM;
    str=next(true,false);
    charnat[asc("+")]=opn;
    if (str=="/") {
      opn=charnat[asc("+")]; // FIXME: Hack (2)
      charnat[asc("+")]=ALPHANUM;
      str=next(true,false);
      charnat[asc("+")]=opn;
      res=tagCreate(">",str,[],li,col);
      li=errorLi(),col=errorCol();
      str=next(true,false);
      if (str!=">") errorSetLiCol(li,col),error("'>' expected [</tag>]");
    }
    else
    if (cdata[str] || str=="code") {
      res=tagCreate("?",str,"",li,col);
      var end="",end2=chr(255);
      if (str=="!--?php") { // Mangled PHP
        end="?-->";res.tag="?php";
      }
      else
      if (str[0]=="?") end="?>"; // e.g. <?php ... ?>
      else
      if (str=="!--") {  // HTML comments
        end="-->";res.tag="!--";
      }
      else
      if (str[0]=="!") end=">"; // e.g. <!DOCTYPE ...>
      else { // e.g. <script language="javascript">
        res.state="<>";
        end="</"+str+">";
        end2="<//"+str+">";
        tagReadAttrs(res);
        if (isTag(res,"script","language",
            function(lang) { return isString(lang) && contains(lang,"html"); }))
        {
          res.state="<";res.elts=[];
          break tag;
        }
      }
      var ptr0=ptr;
      str=cdataNext(end,end2);
      if (str==null && end=="?-->") {
        ptr=ptr0; // FIXME: Doesn't count the #lines correctly, with this
        str=cdataNext(end="?&gt;",""); // Repair the fucking mangling that the FUCKING browsers do with <?php
        if (str!=null) {
          var i=str.indexOf("-->");
          str=str.substring(0,i)+str.substring(i+2,str.length);
        }
      }
      if (str==null) errorSetLiCol(li,col),error("'"+end+"' expected");
      res.elts=str;
      if (buf.substring(ptr,ptr+end.length)==end) ptrNextN(end.length);
      if (buf.substring(ptr,ptr+end2.length)==end2) ptrNextN(end2.length);
    }
    else {
      res=tagCreate("<",str,[],li,col);
      tagReadAttrs(res);
    }
  }
//alert("res="+display(res));
  return res;
}
function htmlTokenize() {
  var s,res=[];
  do {
    s=htmlNext();
    if (s!=null) res.push(s);
  }
  while (s!=null);
  return res;
}

function htmlParse(fpos) {
  if (fpos!=undefined) errorSetPos(fpos.fname,fpos.li,fpos.col);
  var elt,res=[tagCreate("<>","",[])];
  var finished=false;
  var finalize=function() {
  //if (!SERVER/*FIXME: Horrid hack (1)*/) if (empty(res) || elt!=null && last(res).tag==elt.tag) return;
    var closed=res.pop();
    closed.state="<>";
    if (empty(res)) {
      res=closed;
      finished=true;
    }
    else tagAdd(last(res),closed);
  };
  do {
    var li=errorLi(),col=errorCol();
    elt=htmlNext();
    if (elt!=null) {
      var scripth=isTag(elt,"script","language",
                        function(lang) { return isString(lang) && contains(lang,"html"); });
      if (!scripth && (isCDataTag(elt) && !isHtmlComment(elt) || elt.tag=="code"/*FIXME: Hack*/)) {
        elt.elts=scriptTokenize(elt.elts);
      }
      if (!scripth && (isString(elt) || isSelfTag(elt) || elt.tag=="code"/*FIXME: Hack*/)) {
        tagAdd(last(res),elt);
      }
      else
      if (isOpeningTag(elt)) res.push(elt);
      else
      if (isClosingTag(elt)) {
        var closed=null;
        while (last(res).tag!=elt.tag) { // FIXME: Should detect closing tags without the corresponding opening tag
          closed=res.pop();
          if (closed.state=="<" && !isOptCloseTag(closed)) {
            errorSetLiCol(closed.licol[0],closed.licol[1]);
            error("Missing closing tag for <"+closed.tag+">");
          }
          if (last(res)==null) errorSetLiCol(li,col),error("Lone closing tag"); //alert(display(elt.tag));
          tagAdd(last(res),closed);
        }
        finalize();
      }
      else errorSetLiCol(li,col),error("Unknown case"); // Not reached
    }
  //else if (!SERVER/*FIXME: Horrid hack (2)*/) finalize();
  }
  while (elt!=null && !finished);
  if (htmlNext()!=null) error("Markup found past end");
  errorResetPos();
  return res[0];
}

function htmlDisplay(o) {
  displaying=[o.parent];
  var res=display(o);
  displaying=[];
  return res;
}
function scriptRaw(code) {
  var res="";
  for (var i=0;i<code.length;i++) {
    res+=code[i];
  }
  return res;
}
function htmlSerializePlainText(html,prewrap) {
  return htmlSerialize(html,true,prewrap);
}
function htmlSerialize(html,plain/*FIXME: Find a fix for this horrid hack*/,prewrap) {
  if (plain==undefined) plain=false;
  var res="";
  if (plain && isDictionary(html) && (html.tag=="?php" || html.tag=="script")/*here we have to check the attributes*/) {
    res=htmlPretty(html);
  }
  else
  if (isDictionary(html)) {
    if (html.tag!="") {
      res="<"+html.tag;
      var attrs=html.attrs;
      if (!empty(attrs)) {
        for (var n in attrs) {
          if (n[0]!="$"/* FIXME: Hack*/) res+=" "+n+"="+attrs[n];
        }
      }
      if (!isHtmlComment(html) && !isXmlPI(html)) {
        res+=(isSelfClosingTag(html)?"/":"")+">";
      }
    }
    var elt=html.elts;
    if (html.tag=="!--") res+=elt;
    else
    if (html.tag=="script"/*the remaining script tags*/) res+=scriptRaw(elt);
    else
    if (elt) {
      for (var i=0;i<length(elt);i++) {
        res+=htmlSerialize(elt[i],plain,prewrap);
      }
    }
    if (html.tag!="") {
      if (isClosedTag(html)) res+="</"+html.tag+">";
      if (isHtmlComment(html)) res+="-->";
      if (isXmlPI(html))
        if (html.tag[0]=="!"/*FIXME: crap hack for <!DOCTYPE ...>*/) res+=">"; else res+="?>";
    }
    if (!elt) res="";
  }
  else
  if (isString(html)) {
    if (prewrap && contains(html,"\n")) { html=replaceAll(html,"\n","<br>"); }
    res=html;
  }
  return res;
}
