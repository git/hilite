/*
 * pretty.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Pretty (HTML)
function htmlEscapeChars(html) {
  var res="";
  for (var i=0;i<html.length;i++) {
    var c=html[i];
    if (c=='<') c="&lt;";
    if (c=='>') c="&gt;";
    if (c=='&') c="&amp;";
    res+=c;
  }
  return res;
}
function htmlEscapeBlanks(html) {
  var res="";
  for (var i=0;i<html.length;i++) {
    var c=html[i];
    if (c=='\n') c="<br>";
    if (c==' ') c="<span style=\"white-space:pre-wrap;\"> </span>"; //"&nbsp;"; // TODO: Make the generated HTML code shorter (with an <spc> tag, styled with CSS)
    res+=c;
  }
  return res;
}
function htmlSpan(categ,txt) {
  return "<span class=\""+categ+"\">"+txt+"</span>";
}

function htmls(cla) { // FIXME: Hack
  return (scriptStyleName=="plain"?"p":"")+"html "+cla;
}
function attrVal(val) {
  if (val==undefined || val==null) val="";
  return trim(trim(trim(val,"'",true,true),"\"",true,true)," ",true,true);
}
function attrLang(lang) { // FIXME: Remove that, the parser should not know "inactive"
  lang=attrVal(lang);
  lang=trim(replaceAll(lang,"inactive","")," ",true,true);
  return lang;
}
function htmlPretty(html) {
  var res="";
  if (isDictionary(html)) {
    if (html.tag[0]=="\\" || length(html.tag)>1 && html.tag[1]=="\\") { // FIXME: Hack to be able to escape tags
      html.tag=replaceAll(html.tag,"\\","");
    }
    if (html.tag!="" && html.attrs["foldtag"]==undefined) {
      res=htmlSpan(isHtmlComment(html)?htmls("Comment"):isXmlPI(html)?htmls("xmlPI"):htmls("Tag"),
                   "&lt;"+html.tag);
      var attrs=html.attrs;
      if (!empty(attrs)) {
        for (var n in attrs) {
          if (n[0]!="$"/* FIXME: Hack*/) {
            var val=attrs[n];
            if (!SERVER/*FIXME: hack*/) val=htmlEscapeChars(val);
            res+="&nbsp;"+
                 htmlSpan(htmls("Attr"),n)+
                 htmlSpan(htmls("Tag"),"=")+
                 htmlSpan(htmls("AttrVal"),val);
          }
        }
      }
      if (!isHtmlComment(html) && !isXmlPI(html)) {
        res+=htmlSpan(htmls("Tag"),(isSelfClosingTag(html)?"/":"")+"&gt;");
      }
    }
    var elt=html.elts,
        lang=attrLang(html.attrs["language"]);
    if (html.tag=="!--") res+=htmlSpan(htmls("CommentContent"),elt);
    else
    if (html.tag=="?php") {
      enterScriptPretty("php",scriptStyleName);
      res+=scriptPretty(elt);
      leaveScriptPretty();
    }
    else
    if (html.tag=="script" && lang!="html") {
      if (lang=="") lang="javascript";
      if (!scriptLangExists(lang)) error("Unknown language: "+lang);
      enterScriptPretty(lang,scriptStyleName);
      res+=scriptPretty(elt);
      leaveScriptPretty();
    }
    else
    if (elt) {
      for (var i=0;i<elt.length;i++) {
        res+=htmlPretty(elt[i]);
      }
    }
    if (html.tag!="" && html.attrs["foldtag"]==undefined) {
      if (isClosedTag(html)) res+=htmlSpan(htmls("Tag"),"&lt;/"+html.tag+"&gt;");
      if (isHtmlComment(html)) res+=htmlSpan(htmls("Comment"),"--&gt;");
      if (isXmlPI(html))
        if (html.tag[0]=="!"/*FIXME: crap hack for <!DOCTYPE ...>*/)
          res+=htmlSpan(htmls("xmlPI"),"&gt;");
        else
          res+=htmlSpan(htmls("xmlPI"),"?&gt;");
    }
    if (!elt) res="";
  }
  else
  if (isString(html)) {
    var categ=charIs(html[0],BLANK)?null:htmls("Text"); // FIXME: Quite hacky smelling, somehow ...
    res=htmlEscapeBlanks(html);
    if (html[0]=='"' || html[0]=="'") res=htmlEscapeChars(html);
    if (categ) res=htmlSpan(htmls("Text"),res);
  }
  return res;
}

// Pretty (PHP)
var scriptLang="php",
    scriptStyleName="code",
    scriptStyle=lang[scriptLang]["style"][scriptStyleName]; // e.g.  php, pphp
var
   prettyStack=[];
function scriptLangExists(lan) {
  for (var l in lang) if (l==lan) return true;
  return false;
}
function enterScriptPretty(lan,sty) { // FIXME: Make it generic ;; TODO: Test here that the language actually exists, and then remove the tests about that everywhere else
  prettyStack.push([scriptLang,scriptStyleName]);
  scriptLang=lan;
  scriptStyleName=sty;
  scriptStyle=lang[lan]["style"][sty];
}
function leaveScriptPretty() {
  var lan=prettyStack.pop();
  scriptLang=lan[0];
  scriptStyleName=lan[1];
}
function isScriptVar(s) {
  if (scriptLang=="html") return false;
  return scriptLang=="php"?s[0]=='$':false;
}
function isScriptOp(s) {
  if (scriptLang=="html") return false;
  return lang[scriptLang].op[s.toLowerCase()];
}
function isScriptKeyw(s) {
  if (scriptLang=="html") return false;
  if (!lang[scriptLang].keyw_sensitive) s=s.toLowerCase();
  return lang[scriptLang].keyw[s];
}
function isScriptPred(s) {
  if (scriptLang=="html") return false;
  if (!lang[scriptLang].pred_sensitive) s=s.toLowerCase();
  return lang[scriptLang].pred[s]; // TODO: Verify that the PHP predefs are indeed f$%&/(g case /insensitive/ ...
}
function scriptPrettyVar(tok) {
  if (isScriptVar(tok)) return htmlSpan(scriptStyle+" Variable",tok);
  return tok;
}
function scriptPrettyPredef(tok) {
  if (isScriptPred(tok)) return htmlSpan(scriptStyle+" Predef",tok);
  return tok;
}
function scriptPrettyName(tok) {
  if (isScriptVar(tok)) return htmlSpan(scriptStyle+" Variable",tok);
  if (isScriptPred(tok)) return htmlSpan(scriptStyle+" Predef",tok);
  return tok;
}
function scriptMarkVars(str) {
  res="";
  for (var i=0;i<str.length;i++) {
    if (str[i]!="$" || scriptLang!="php") res+=str[i];
    else {
      var i0=i;
      while (charIs(str[i],ALPHANUM)) i++;
      res+=htmlSpan(scriptStyle+" Variable",str.substring(i0,i));
      i--;
    }
  }
  return res;
}
function scripts(cla) { // FIXME: Hack
  return scriptStyle+" "+cla;
}
function scriptReplaceEscapedSymbs(tok,htmls) {
  if (htmls) {
    tok=replaceAll(tok,"&lt;","<");
    tok=replaceAll(tok,"&gt;",">");
  }
  tok=replaceAll(tok,"\\\\","&zboc;");
  tok=replaceAll(tok,"&zboc;","\\");
  return tok;
}
function scriptPrettyElt(tok) {
  var categ=scripts("Symbol");
  tok=scriptReplaceEscapedSymbs(tok,true);
  if (charIs(tok[0],BLANK)) return htmlEscapeBlanks(tok);
  if (tok[0]=="'" || tok[0]=='"') { tok=scriptMarkVars(htmlEscapeBlanks(htmlEscapeChars(tok)));categ=scripts("ConstString"); }
  if (charIsDigit(tok[0])) { categ=scripts("ConstString"); } // TODO: should we use only "Const" ?
  if (tok.length>=2 && tok[0]=="/" && tok[1]=='*') categ=scripts("Comment");
  if (tok.length>=2 && tok[0]=="/" && tok[1]=='/') {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (tok.length>=1 && tok[0]=="#") {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (tok.length>=1 && tok[0]=="-" && tok[1]=="-" && scriptLang=="sql") {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (isScriptOp(tok)) categ=scripts("Operator");
  if (isScriptKeyw(tok)) categ=scripts("Keyword");
  if (isScriptPred(tok)) categ=scripts("Predef");
  if (tok[0]=="$") categ=scripts("Variable");
  return htmlSpan(categ,tok);
}
function scriptPretty(code) {
  var res="";
  for (var i=0;i<code.length;i++) {
    res+=scriptPrettyElt(code[i]);
  }
  return res;
}
