/*
 * inherit_attrs.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Inheriting attributes
function foreach_dupAttrs(attrs) {
  var res=tagCreate("<","",[]);
  for (var n in attrs.attrs) {
    res.attrs[n]=attrs.attrs[n];
  }
  return res;  
}
function foreach_collectAttrs(attrs,node) {
  if (!isDictionary(attrs) || !isDictionary(node)) error("foreach_collectAttrs");
  var collect=false,res=attrs;
  for (var n in node.attrs) {
    if (n[0]=="$") collect=true;
  }
  if (collect) {
    res=tagCreate("<","",[]);
    for (var n in attrs.attrs) {
      if (n[0]=="$") res.attrs[n]=attrs.attrs[n];
    }
    for (var n in node.attrs) {
      if (n[0]=="$") res.attrs[n]=node.attrs[n];
    }
  }
  return res;
}
function foreach_inheritAttrs(node,attrs) {
  if (!isDictionary(attrs) || !isDictionary(node)) error("foreach_inheritAttrs");
  for (var n in attrs.attrs) {
    if (n[0]=="$" && node.attrs[n]==undefined) node.attrs[n]=attrs.attrs[n];
  }
}
function inherit_attrs(node,attrs) {
  if (isDictionary(node)) {
    var ntag=node.tag;
    if (ntag!="!--") {
      foreach_inheritAttrs(node,attrs);
      attrs2=foreach_collectAttrs(attrs,node);
      var attrs2_0=foreach_dupAttrs(attrs2); // FIXME: There is something fishy, here ; we should NOT have to duplicate attrs2 ; even when we duplicate inside foreach_collectAttrs, it doesn't work. There is a (well) hidden sharing taking place somewhere ...
      var elt=node.elts;
      for (var i=0;i<elt.length;i++) {
        if (!isString(elt[i])) {
          inherit_attrs(elt[i],attrs2_0);
        }
      }
    }
  }
}
