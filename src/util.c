/*
 * util.c
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <termios.h>
#include <sys/stat.h>
#include <dirent.h>

/* TODO: Implement Non blocking getchar()
struct termios TERM0,TERM1;*/
int setterm(int reset) {
/*if (!reset) {
    tcgetattr(0,&TERM0); 
    TERM1=TERM0;
    TERM1.c_lflag&=~ICANON;
    TERM1.c_lflag&=~ECHO;
    TERM1.c_lflag&=~ISIG;
    TERM1.c_cc[VMIN]=0;
    TERM1.c_cc[VTIME]=0;
    tcsetattr(0,TCSANOW,&TERM1);
  }
  else {
    tcsetattr(0, TCSANOW, &TERM0);
  }*/
}
int getkey() {
/*while (1) {
    int c=getchar();
    if (c!=-1) return c;
  }
  return -1; // Not reached*/
  return getchar();
}

// Error
void enter() {
  setterm(0);
}
static int leaveWait=0;
void leave(int res,int cr) {
  if (leaveWait) printf("-- Press Enter --"),getkey();
            else if (cr) printf("\n");
  setterm(1);
  exit(res);
}
void error(char *msg) {
  printf("%s\n",msg);
  leave(1,0);
}

// Strings
int ucase(int c) {
  if (c>='a' && c<='z') c-='a',c+='A';
  return c;
}
int strucmp(char *s1,char *s2) {
  while (*s1 && *s2) {
    if (ucase(*s1)!=ucase(*s2)) break;
    s1++,s2++;
  }
  if (*s1 || *s2) return 1;
             else return 0;
}
int uendsWith(char *s,char *end) {
  int ls=strlen(s),le=strlen(end);
  if (le>ls) return 0;
  s+=ls-le;
  return !strucmp(s,end);
}
int ncharsTo(char *s,int c) {
  int n=0;
  while (*s && *s!=c) s++,n++;
  return n;
}
char *strdup2(char *s) {
  char *res=(char*)malloc(strlen(s)+1);
  strcpy(res,s);
  return res;
}

// Files
typedef struct file {
  char *pathname;
  int size,isdir;
  struct file **lst;
}
file;

file *newfile(char *pathname,int size,int n) {
  file *f=(file*)malloc(sizeof(file));
  f->pathname=strdup2(pathname);
  f->size=size;
  f->isdir=n!=-1;
  f->lst=NULL;
  if (n!=-1) {
    f->lst=(file**)malloc((n+1)*sizeof(file*));
    int i;
    for (i=0;i<=n;i++) f->lst[i]=NULL;
  }
  return f;
}
int isdir(char *pathname) {
  struct stat ST;
  int res;
  res=stat(pathname,&ST);
  if (res!=0) return 0;
  return S_ISDIR(ST.st_mode)!=0;
}
int fsize(char *pathname) {
  struct stat ST;
  int res;
  res=stat(pathname,&ST);
  if (res!=0) return 0;
  return ST.st_size;
}
char *fpath(char *path,char *name) {
  char *s=(char*)malloc(strlen(path)+strlen(name)+2);
  strcpy(s,path);
  strcat(s,"/");
  strcat(s,name);
  return s;
}
char *loadfile0(char *pathname,int size) {
  char *res=(char*)malloc(size+100);
  res[size]=0;
  FILE *F=fopen(pathname,"r");
  fread(res,1,size,F);
  fclose(F);
  return res;
}
char *loadfile(file *f) {
  return loadfile0(f->pathname,f->size);
}
char *fload(char *pathname) {
  return loadfile0(pathname,fsize(pathname));
}
file *findfile(file *dir,char *fname) {
  for (file **ptr=dir->lst;*ptr;ptr++) {
    if (uendsWith((*ptr)->pathname,fname)) return *ptr;
  }
  return NULL;
}

// Recursively traversing directories
file *traverse(char *pathname,char *mask) {
//printf("fileLoading %s ...\n",pathname);
  file *f=NULL;
  if (isdir(pathname)) {
    DIR *d=opendir(pathname);
    if (d==NULL) error("traverse");
    int n=0;
    struct dirent *e=readdir(d);
    while (e!=NULL) {
      if (strcmp(e->d_name,".") && strcmp(e->d_name,"..")) n++;
      e=readdir(d);
    }
    f=newfile(pathname,0,n);
    int i=0;
    rewinddir(d);
    e=readdir(d);
    while (e!=NULL) {
      if (strcmp(e->d_name,".") && strcmp(e->d_name,"..")) {
        char *FNAME=fpath(pathname,e->d_name);
        file *F2=traverse(FNAME,mask);
        if (F2!=NULL) {
          f->lst[i]=F2;
          i++;
        }
        free(FNAME);
      }
      e=readdir(d);
    }
    closedir(d);
  }
  else {
    if (mask==NULL || uendsWith(pathname,mask)) f=newfile(pathname,fsize(pathname),-1);
  }
  return f;
}
void indent(int n) {
  while (n--) printf(" ");
}
void display(file *f) {
  static int ind=0;
  if (f->isdir) {
    printf("d");
    indent(ind);
    printf("  %s_\n",f->pathname);
    ind+=2;
    for (file **ptr=f->lst;*ptr;ptr++) {
      display(*ptr);
    }
    ind-=2;
  }
  else {
    printf("f");
    indent(ind);
    printf("  %s %d_\n",f->pathname,f->size);
  }
}

// Argvs
void argvCut(int argc,char **argv,int j) {
  int n=strlen(argv[j]),i;
  for (i=0;i<n;i++) if (argv[j][i]=='\\') argv[j][i]='/';
  while (n>0) {
    if (argv[j][n]=='/') break;
    n--;
  }
  argv[j][n]=0;
}
void argvPrint(int argc,char **argv) {
  int first=1,i;
//printf("[%d]",argc);
  for (i=0;i<argc;i++) {
    if (first) first=0;
          else printf(" ");
    printf("%s",argv[i]);
  }
}
void argvRemove(int *argc,char **argv,int j) {
  for (int i=j;i+1<*argc;i++) {
    argv[i]=argv[i+1];
  }
  (*argc)--;
}
int argvFind(int argc,char **argv,char *s) {
  for (int i=0;i<argc;i++) {
    if (!strcmp(argv[i],s)) return i;
  }
  return -1;
}
