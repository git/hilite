/*
 * tokenizer.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Tokenizer
var UNKNOWN=0,ALPHANUM=1,OPCHAR=2,SPLITTER=3,BLANK=4,QUOTE=5,DQUOTE=6;
var charnat=[];

function charIs(ch,nat) {
  return charnat[asc(ch)]==nat;
}
function charIsDigit(ch) {
  return ch=="1" || ch=="2" || ch=="3" || ch=="4" || ch=="5"
      || ch=="6" || ch=="7" || ch=="8" || ch=="9" || ch=="0";
}

var buf,ptr;
function bufEof() {
  return ptr>=buf.length;
}

var _ptrNextCount=true;
function ptrNext() {
  var li=errorLi(),col=errorCol();
  if (buf[ptr]=='\n') li++,col=1; else col++;
  if (_ptrNextCount) errorSetLiCol(li,col);
  ptr++;
}
function ptrNextN(n) {
  while (n--) ptrNext();
}
function find(nat) {
  while (ptr<buf.length && !charIs(buf[ptr],nat)) ptrNext();
}
function findChar(ch) {
  while (ptr<buf.length && buf[ptr]!=ch) ptrNext();
}
function findString(str) {
  while (ptr<buf.length+str.length-1 && buf.substring(ptr,ptr+str.length)!=str) ptrNext();
  if (ptr>=buf.length+str.length-1) ptr=buf.length; // FIXME: Doesn't do the appropriate calls to ptrNext()
}
function skip(nat) {
  while (ptr<buf.length && charIs(buf[ptr],nat)) ptrNext();
}
function isHtmlEscapedChar() {
  var i=ptr,res=false;
  if (buf[i]=="&") {
    i++;
    while (i<buf.length && charIs(buf[i],ALPHANUM)) i++;
    if (i<buf.length && buf[i]==";") res=true;
  }
  return res;
}
function next(skipblanks,ignoreSQuote/*FIXME: Hack*/) {
  if (skipblanks) skip(BLANK);
  var res=null;
  if (ptr<buf.length) {
    var ptr0=ptr;
    if (buf.substring(ptr,ptr+2)=="/*") {  // TODO: Finish Javascript & PHP comments
      var li=errorLi(),col=errorCol();
      findString("*/");
      if (ptr>=buf.length) errorSetLiCol(li,col),error("Unterminated comment");
      ptrNextN(2);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (buf.substring(ptr,ptr+2)=="//") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (!SERVER/*FIXME: Hack*/ && buf.substring(ptr,ptr+1)=="#") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (!SERVER/*FIXME: Hack*/ && scriptLang=="sql" && buf.substring(ptr,ptr+2)=="--") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (buf.substring(ptr,ptr+1)=="&" && isHtmlEscapedChar()) {
      var li=errorLi(),col=errorCol();
      findString(";");
      ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],SPLITTER)) res=buf[ptr],ptrNext();
    else
    if (charIs(buf[ptr],BLANK)) {
      skip(BLANK);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],QUOTE) && ignoreSQuote) res=buf[ptr],ptrNext();
    else
    if (charIs(buf[ptr],ALPHANUM)) {
      skip(ALPHANUM);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],OPCHAR)) {
      skip(OPCHAR);
      res=buf.substring(ptr0,ptr);
    }
    else {
      var li=errorLi(),col=errorCol();
      for (var q=QUOTE;q<=DQUOTE;q++) if (ptr<buf.length && charIs(buf[ptr],q)) {
        ptrNext();
        find(q);
        if (ptr>=buf.length) errorSetLiCol(li,col),error("Unterminated string");
        ptrNext();
        res=buf.substring(ptr0,ptr);
      }
      if (res==null) errorSetLiCol(li,col),error("Unknown element: "+buf[ptr]+";;"+charnat[asc(buf[ptr])]);
    }
  }
//alert(res);
  return res;
}
function tokenize() {
  var s,res=[];
  do {
    s=next(false,false);
    if (s!=null) res.push(s);
  }
  while (s!=null);
  return res;
}
function bufstart(txt) {
  buf=txt;
  ptr=0;
}

// Tokenizing scripts
function scriptTokenize(code) {
  var oldbuf=buf,oldptr=ptr,oldfpos=errorPos();
  bufstart(code);
  var s,res=[];
  do {
    s=next(false,false);
    if (s!=null) res.push(s);
  }
  while (s!=null);
  buf=oldbuf;ptr=oldptr;
  if (oldfpos!=null) errorSetPos(oldfpos.fname,oldfpos.li,oldfpos.col);
  return res;
}
