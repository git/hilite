/*
 * hilite_cli.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// TODO: Add options for controlling the indent level, adding line numbers, etc.
// Hilite script
function hilite_scriptRaw(lang,style,text) {
  var res="";
  if (lang=="") lang="html";
  enterScriptPretty(lang,style);
  if (isString(text)) {
    hilite_init();
    text=trim(text,"\n",1,0);
    text=trim(text," \n",0,1);
    bufstart(text);
    if (lang=="html") res=htmlPretty(htmlParse());
                 else res=scriptPretty(tokenize());
  }
  else {
    text.elts=arrayTrim(text.elts,true,true);
    if (lang=="html") res=htmlPretty(text);
                 else res=scriptPretty(text.elts);
  }
  leaveScriptPretty();
  return res;
}
function hilite_scriptCooked(lang,style,text) {
  if (isString(lang) && startsWith(lang,"inactive")) { // Inactive tags: we replace
    lang=lang.substring(9,lang.length);
    return "<code><table><tr><td class=\"hilite\">"+hilite_scriptRaw(lang,style,text)+
           "</td></tr></table></code>";
  }
  else return "";
}
function hilite_script(attrs,text) {
  return hilite_scriptCooked(attrVal(attrs["language"]),"code",text);
}

// Hilite hilite
var hilite_plaintextAttrs=transformCreate(),
    hilite_plaintextMarkup=transformCreate();
function attrLang(lang) {
  lang=attrVal(lang);
  lang=trim(replaceAll(lang,"vars","")," ",true,true);
  lang=trim(replaceAll(lang,"predefs","")," ",true,true);
  lang=trim(replaceAll(lang,"names","")," ",true,true);
  lang=trim(replaceAll(lang,"inactive","")," ",true,true);
  return lang;
}
function blockCla(tag,cla) {
  if (cla==null) cla="";
  cla=attrVal(cla).split(" ");
  var i=index(cla,"inline");
  if (i!=-1) cla[i]="Inline";
  else {
    i=index(cla,"block");
    if (i!=-1) cla[i]="Block";
    else cla.push(tag=="code"?"Block":"Inline");
  }
  return "\""+cla.join(" ")+"\"";
}
function plainLang(node,lang) {
  var tag=node.tag;
  node.tag="span";
  var node2=tagCreate("<","",[]);
  node2.elts=node.elts;
  var html=hilite_scriptRaw(lang,"plain",htmlSerialize(node2));
  node.elts=[html];
  var cla=node.attrs["class"];
  node.attrs["class"]=blockCla(tag,cla);
  return node;
}
function hilite_plaintextInit() {
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    var lang=attrVal(node.attrs["language"]);
    if (lang!="") node.attrs["$hilang"]='"'+lang+'"';
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="var" || node.tag=="vars") {
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=false;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="predef" || node.tag=="predefs") {
      node.attrs["$hivars"]=false;
      node.attrs["$hipredefs"]=true;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="name" || node.tag=="names") {
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=true;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="ltt") { // FIXME: Why can't <ltt>...</ltt> be in hilite_plaintextMarkup ?
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=false;
      var lang=attrLang(node.attrs["$hilang"]);
      foreach_text(node,function(node,attrs) {
        var res=node,tt=false;
        if (node!="," && node!=";" && !charIs(node,BLANK)) tt=true;
        if (tt) {
          var s=res;
          res=tagCreate("<>","tt",[]);
          if (lang!="") enterScriptPretty(lang,"plain");
          res.attrs["class"]="\""+scriptStyle+" ListOfTT\"";
          if (lang!="") leaveScriptPretty();
          res.attrs["$hivars"]=true; // TODO: Check if this is necessary
          res.elts.push(s);
        }
        return res;
      });
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="script" || node.tag=="code" || node.tag=="c"
     || contains(node.tag,"htm") || contains(node.tag,"css")
     || contains(node.tag,"js") || contains(node.tag,"php") || contains(node.tag,"sql")) // FIXME: Unify the place where all these language names are defined ...
    {
      node.attrs["$hivars"]=false;
      node.attrs["$hipredefs"]=false;
    }
    return node;
  });
  
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    var tag=node.tag;
    if (tag=="c" || tag=="code") {
      var lang=attrLang(node.attrs["$hilang"]);
      node=plainLang(node,lang);
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="htm" || node.tag=="htm+code" || node.tag=="code+htm") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"html");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="css" || node.tag=="css+code" || node.tag=="code+css") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"css");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="js" || node.tag=="js+code" || node.tag=="code+js") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"javascript");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="php" || node.tag=="php+code" || node.tag=="code+php") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"php");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="sql" || node.tag=="sql+code" || node.tag=="code+sql") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"sql");
    }
    return node;
  });

  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="script") {
      node.attrs["foldtag"]=1;
      node.elts=arrayTrim(node.elts,true,true);
      node=tagCreate("<>","td",[node]);
      node.attrs["class"]="\"hilite\"";
      node=tagCreate("<>","tr",[node]);
      node=tagCreate("<>","table",[node]);
      node=tagCreate("<>","code",[node]);
      foreach_cut();
    }
    return node;
  });
}

function hilite_plaintextNames(node,attrs) {
  foreach_text(node,function(node,attrs) {
    var res=node;
    if (length(node)>1 && node[0]=="\\" && node[1]!="\\") res=node.substring(1,node.length);
    res=scriptReplaceEscapedSymbs(res,false);
    if (isScriptVar(node) && attrs.attrs["$hivars"]) res=scriptPrettyVar(node);
    else
    if (isScriptPred(node) && attrs.attrs["$hipredefs"]) res=scriptPrettyPredef(node);
    return res;
  });
  return node;
}
var hilite_prewrap=false;
function hilite_hilite(attrs,text) {
//alert(display(text));
  var lang=attrVal(attrs["language"]),res;
  if (isString(lang) && startsWith(lang,"inactive")) res=hilite_script(attrs,text);
  else {
    var vars=true,predefs=false;
    if (lang==undefined || lang==null || lang=="") lang="html";
    else {
      if (contains(lang,"vars")) lang=trim(replaceAll(lang,"vars","")," ",true,true),vars=true;
                                                                                else vars=false;
      if (contains(lang,"predefs")) lang=trim(replaceAll(lang,"predefs","")," ",true,true),predefs=true;
      if (contains(lang,"names")) lang=trim(replaceAll(lang,"names","")," ",true,true),vars=predefs=true;
    }
    if (!scriptLangExists(lang)) error("Unknown language: "+lang+"[["+attrs["language"]+"]]");
    var tree;
    if (isString(text)) {
      hilite_init();
      bufstart(text);
      tree=htmlParse();
    }
    else tree=tagCreate(text.state,"",text.elts);
    tree.attrs["$hilang"]=lang;
    inherit_attrs(tree,tree);
    transform(tree,hilite_plaintextAttrs);
    tree.attrs["$hivars"]=vars;
    tree.attrs["$hipredefs"]=predefs;
    inherit_attrs(tree,tree);
    enterScriptPretty(lang,"plain");
    hilite_plaintextNames(tree,tree);
    leaveScriptPretty();
  //alert(display(tree));
    transform(tree,hilite_plaintextMarkup);
    if (hilite_prewrap) tree.elts=arrayTrim(tree.elts,true,true);
    res=htmlSerializePlainText(tree,hilite_prewrap); // TODO: default language should be HTML here (?)
  }
  return res;
}

// Init
function hiliteInit(prewrap) {
  hilite_plaintextInit();
  hilite_prewrap=prewrap;
  dom_transform("hilite",hilite_hilite);
  dom_transform("script",hilite_script);
  window.document.body.style.display="block";
}
