/*
 * jsmods.c
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.c"

// jsmods
char *header=NULL;
void jshead(FILE *out,char *fname) {
  if (header==NULL) return;
  char *txt=header;
  int n=strlen(txt);
  for (int i=0;i<n;i++) {
    if (!strncmp(txt+i,"$fname",6)) {
      fprintf(out,"%s",fname);
      i+=5;
    }
    else fprintf(out,"%c",txt[i]);
  }
}
void jsout(FILE *out,char *txt,int n) {
  int instr=0;
  for (int i=0;i<n;i++) {
    if (instr) {
      if (txt[i]=='\\') {
        fprintf(out,"%c",txt[i++]);
      }
      else
      if (txt[i]=='"') instr=0;
      fprintf(out,"%c",txt[i]);
    }
    else
    if (txt[i]=='"') {
      instr=1;
      fprintf(out,"%c",txt[i]);
    }
    else
    if (!strncmp(txt+i,"/*",2)) {
      i+=2;
      while (i<n && strncmp(txt+i,"*/",2)) i++;
      if (i<n) i++;
    }
    else
    if (!strncmp(txt+i,"//",2)) {
      i+=2;
      while (i<n && txt[i]!='\n') i++;
      if (i<n) i--;
    }
    else fprintf(out,"%c",txt[i]);
  }
}
void jsmod(char *dest,file *dir,file *f) {
  static char buf[1000];
  strcpy(buf,dest);
  strcat(buf,"/");
  char *fname=buf+strlen(buf);
  strcat(buf,strdup2(f->pathname+strlen(dir->pathname)+1));
  int len=strlen(buf);
  strcpy(buf+len-3,"js");
//printf("[%s]\n",buf);
  FILE *out=fopen(buf,"w");
  jshead(out,fname);
  char *txt=loadfile(f);
  int instr=0;
  for (int i=0;i<f->size;i++) {
    if (instr) {
      if (txt[i]=='\\') {
        fprintf(out,"%c",txt[i++]);
      }
      else
      if (txt[i]=='"') instr=0;
      fprintf(out,"%c",txt[i]);
    }
    else
    if (txt[i]=='"') {
      instr=1;
      fprintf(out,"%c",txt[i]);
    }
    else
    if (!strncmp(txt+i,"include ",8)) {
      char *s,*txti;
      len=ncharsTo(txt+i,'\n');
      s=(char*)malloc(len-10);
      s[len-11]=0;
      strncpy(s,txt+i+9,len-11);
      file *fi=findfile(dir,s);
      txti=loadfile(fi); 
    //fprintf(out,"%s\n",txti);
      jsout(out,txti,fi->size);
      i+=len;
    }
    else
    if (!instr && !strncmp(txt+i,"/*",2)) {
      i+=2;
      while (i<f->size && strncmp(txt+i,"*/",2)) i++;
      if (i<f->size) i++;
    }
    else
    if (!instr && !strncmp(txt+i,"//",2)) {
      i+=2;
      while (i<f->size && txt[i]!='\n') i++;
      if (i<f->size) i--;
    }
    else fprintf(out,"%c",txt[i]);
  }
  fclose(out);
}
void jsmods(char *dest,file *f) {
//printf("{%s}\n",f->pathname);
  for (file **ptr=f->lst;*ptr;ptr++) {
    if (uendsWith((*ptr)->pathname,".prj")) {
      jsmod(dest,f,*ptr);
    }
  }
}

// main
void help() {
  printf("Usage: jsmods [-header HEADER][SRC][DEST]\n");
}
int main(int argc,char **argv) {
  enter();
  int i,res=0;
  if ((i=argvFind(argc,argv,"-header"))!=-1) {
    argvRemove(&argc,argv,i);
    header=argv[i];
    argvRemove(&argc,argv,i);
  }
  if (argc>1 && (!strcmp(argv[1],"-h") || *argv[1]=='-')) help();
  else
  if (argc<=2) help(),res=1;
  else {
  //printf("<%s %s %s>\n",header,argv[1],argv[2]);
    header=fload(header);
    file *f=traverse(argv[1],NULL);
    //display(f);
    jsmods(argv[2],f);
  }
  leave(res,0);
  return 0; // Not reached
}
