<!--
  Copyright (C) 2017  Sebastian Heimpel and Henri Lesourd

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3 or any
  later version published by the Free Software Foundation; with no
  Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy
  of the license can be found in the file COPYING, in the folder "web/" of
  your hilite.js installation.
-->
<html>
  <head>
    <title>hilite.js</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="hilite.css" type="text/css" media="screen">
    <script src="hilite.js"></script>
  </head>
  <body>
    <table width="100%" border=0>
    <tr>
      <td valign=top width="85%">
        <font size=0>&nbsp;</font>
        <center>
          <font size=40><tt><span style="background-color:FF0000;">h</span><span style="background-color:#00C000;">i</span><span style="background-color:#0000FF;">l</span><span style="background-color:#FFFF00;">i</span><span style="background-color:#FF00FF;">t</span><span style="background-color:#00FFFF;">e</span><span style="background-color:#FF8080;">.</span><span style="background-color:#80F080;">j</span><span style="background-color:#8080FF;">s</span></tt></font><br>
          <i>Highlighting the web</i>
          <br><br>
          (C) Sebastian Heimpel &amp; Henri Lesourd, 2017
        </center>
      </td>
      <td></td>
    </tr>
    <tr>
      <td valign=top align=justify style="padding: 60px 60px 0px 40px;">
        <i style="font-size: 32px; background-color:#E0E0E0;">Rationale</i><br>

        <tt>hilite.js</tt> is a concise<!--,--> and robust<!--, and extensible--> syntax highlighting library
        for the web.

        <ul>
          <li>
          First of all, it is <i>concise</i>: built on top of a small set of simple components,
          chiefly an HTML parser and a set of Javascript routines to traverse parse trees, its
          core is less than 1000 lines of code, and is not likely to grow much more in the
          future.<br><br>
          </li>

          <li>
          Next, it is <i>robust</i>: we use the appropriate algorithms to parse the pages we
          highlight (this includes the possibility to highlight HTML markup as such), thus lest
          browser mangling prevents it, there are no situations where the highlighting of correct
          markup and/or script should fail.
          <br><br>

          <!--On the other hand, <tt>hilite.js</tt> includes a validator,
          which can be used to find errors in your pages (this validator has to run outside
          of the browser, for to our knowledge, modern browsers provide no way to know for
          sure the position of a given tag in the HTML source).
          <br><br>
          </li>

          <li>
          Finally, the set of languages that are known to the highlighter is <i>extensible</i>
          by means of very simple <i>language description</i> files.-->
          </li>
        </ul>
        
        <tt>hilite.js</tt> is free software, released under the GPLv3.
        
        <br><br>
        <br>

        <span style="font-size: 32px; background-color:#E0E0E0;"><i>Using</i> <tt>hilite.js</tt></span><br>
        In <tt>hilite.js</tt>, syntax highlighting can be conveniently done by the author
        by means of a number of tags, that are used to markup the text where highlightings
        are desired.
        <br><br>
        There are two main categories of tags:
        <ul>
          <li>
          Those which are used to perform syntax coloring of pure code blocks ;
          <br><br>
          </li>
          <li>
          Those which are used to perform <i>plaintext highlighting</i> of a section of
          text mixing code elements (e.g., variables, predefined symbols, etc.) and natural
          language.
          <br><br>
          In plaintext highlighting, the code elements are automatically recognized by the
          highlighter, there is no need to mark each one of them individually.
          </li>
        </ul>

        We have (see <span style="background-color:#E0E0E0; color:black;"><i><a href="#AppendixA" style="text-decoration: none;">Appendix A</a></i></span> for a more complete description of all the available tags):<br>
        <hilite>
        <table border=1>
          <tr><td colspan=2 style="background-color:#E0E0E0;"><i>Code highlighting</i></td>
          </tr>
          <tr>
            <td width="1%">
              <htm><code language="$LANG">...&nbsp;&nbsp;
</code>
              </htm>
            </td>
            <td>Plaintext code highlighting, according to <span class="phtml AttrVal">$LANG</span> (block style)
            </td>
          </tr>
          <tr>
            <td>
              <htm><c language="$LANG">...
</c>
              </htm>
            </td>
            <td>Plaintext code highlighting, according to <span class="phtml AttrVal">$LANG</span> (inline style)
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><htm>...</htm>
              </htm>
            </td>
            <td>HTML markup ; same as:<br> <htm><c language="html">...</c></htm>
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><php>...</php>
              </htm>
            </td>
            <td>PHP code ; same as:<br> <htm><c language="php">...</c></htm>
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><js>...</js>
              </htm>
            </td>
            <td>Javascript code ; same as:<br> <htm><c language="javascript">...</c></htm>
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><sql>...</sql>
              </htm>
            </td>
            <td>PL/SQL code ; same as:<br> <htm><c language="sql">...</c></htm>
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><css>...</css>
              </htm>
            </td>
            <td>CSS code ; same as:<br> <htm><c language="css">...</c></htm>
            </td>
          </tr>
        </table>
        <br>
        <table border=1>
          <tr><td colspan=2 style="background-color:#E0E0E0;"><i>Plaintext highlighting</i></td>
          </tr>
          <tr>
            <td valign=top>
              <span class="phtml Tag">&lt;ltt></span>...<span class="phtml Tag">&lt;/ltt></span>
            </td>
            <td>List of TTs, with highlighting of <tt>$</tt>-prefixed language variables (PHP only)
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><vars>...</vars></htm>
            </td>
            <td>Plaintext highlighting of language variables (PHP only)
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><predefs>...</predefs></htm>&nbsp;&nbsp;
            </td>
            <td>Plaintext highlighting of predefined symbols
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><names>...</names></htm>
            </td>
            <td>Same as:<br>
                <htm><vars><predefs>...</predefs>
</vars>
                </htm>
            </td>
          </tr>
        </table>
        </hilite>
        <br>
        To use <tt>hilite.js</tt> in a page, we only need to include it (plus the appropriate stylesheet) in the header, e.g.:
        <script language="inactive html">
<html>
  <head>
    <\script src="hilite.js"></\script>
    <link href="hilite.css" type="text/css">
  </head>
  <body>
  ...
  </body>
</html>
</script>

        <br>
        <hilite>
        The <htm><hilite>...</hilite></htm> tag may then be used in the body, e.g. the following snippet:
        <script language="inactive html">
<hilite>
  <php>
foreach ($_SERVER AS $key=>$val) {
echo "$key=$val<br>";
}
  </php>
</hilite>
        </script>    
        </hilite>

        <br>
        will be highlighted as:
      <table border=1><tr><td>
  <hilite>
      <php>
foreach ($_SERVER AS $key=>$val) {
  echo "$key=$val<br>";
}
      </php>
  </hilite>
      </td></tr></table>
  <br>
  
        <hilite>
For plaintext highlighting, we use the related tags, for example by means of the <htm><names>...</names></htm> tag, the following:</hilite>
        <script language="inactive html">
<hilite language="php">
  <names>
    <c>list( array $parentID, array $parentLV ) = arrlevel( string $tabk, integer $k, integer $ipd );</c>
    <br><br>

    Parameters:<br>
    $tabk - name of the categories' table<br>
    $k - id of the actual category<br>
    $idp - id of parent category<br>
    <br>

    Return values:<br>
    array $parentID - the categories' ids<br>
    array $parentLV - the categories' horizontal levels<br>
  </names>
</hilite>
        </script>

  <br>
  will be rendered (mostly) without any manual markups:<br>
  <table border=1><tr><td>
    <hilite language="php">
    <names>
      <c>list( array $parentID, array $parentLV ) = arrlevel( string $tabk, integer $k, integer $ipd );</c>
      <br><br>

      Parameters:<br>
      $tabk - Name of the categories' table<br>
      $k - id of the actual category<br>
      $idp - id of parent category<br>
      <br>

      Return values:<br>
      array $parentID - the categories' ids<br>
      array $parentLV - the categories' horizontal levels<br>
    </names>
    </hilite>
  </td></tr></table>

        <br><br>
        <font size=0><br></font>

        <span style="font-size: 32px; background-color:#E0E0E0;"><i>Styling</i> <tt>hilite.js</tt></span><br>
        <hilite>
        <br>
        When it highlights a code or a markup snippet, <tt>hilite.js</tt> slices it into tokens, and then
        embeds these tokens inside <htm><span>...</span></htm> tags, using the appropriate styles. For example,
        the following PHP snippet:<br>
        <script language="inactive html">
<php>
echo "Hello!"; // This is a PHP comment
</php>
        </script>
        
        <br>
        is highlighted as:
        <table border=1><tr><td>
<php>
echo "Hello!"; // This is a PHP comment
</php>
        </td></tr></table>

        <br>
        by means of the following markup:<br>
        <table border=1><tr><td>
<htm>
<span class="pphp Keyword">echo</span>
<span class="pphp ConstString">"Hello!"</span>
<span class="pphp Operator">;</span></htm><br>
<span class="phtml Tag">&lt;span</span><span class="phtml Attr"> class</span><span class="phtml Tag">=</span><span class="phtml AttrVal">"pphp Comment"</span><span class="phtml Tag">></span><span class="pphp Comment">/</span><span class="pphp Comment">/ This is a PHP comment</span><span class="phtml Tag">&lt;/span><br></span>
        </td></tr></table>
        </hilite>

        <br>
        To customize the highlighting, the user may define the CSS classes <tt><i>pphp</i></tt>,
        <tt><i>Keyword</i></tt>, <tt><i>ConstString</i></tt>, etc. that can be seen in the HTML
        snippet above as he pleases (the list of the CSS classes used by the highlighter, along with
        their respective meanings, is given in
        <span style="background-color:#E0E0E0; color:black;"><i><a href="#AppendixB" style="text-decoration: none;">Appendix B</a></i></span>).
        
        <br><br>
        <br>

        <a name="AppendixA"/>
        <span style="font-size: 32px; background-color:#E0E0E0;"><i>Appendix A</i>: markup reference</span><br>
        <br>
        <hilite>
        <table width="100%" border=1>
          <tr><td style="background-color:#E0E0E0;"><i>Code highlighting</i></td>
          </tr>
          <tr><td>
            <htm><code language="$LANG" class="inline|block">...</code></htm>
            <br><br>
            Plaintext highlighting of code, according to the language given in <span class="phtml AttrVal">$LANG</span>.
          </td></tr>
          <tr><td>
            <htm><c language="$LANG">...</c></htm>
            <br><br>
            Same as:<br>
            <htm><code language="$LANG" class="inline">...</code></htm>.
          </td></tr>
        </table>
        <br>
        <table width="100%" border=1>
          <tr><td style="background-color:#E0E0E0;"><i>Inline code highlighting (abbreviations)</i></td>
          </tr>
          <tr><td>
            <htm><htm>...</htm></htm> <b>=&gt;</b> <htm><c language="html">...</c></htm>
          </td></tr>
          <tr><td>
            <htm><php>...</php></htm> <b>=&gt;</b> <htm><c language="php">...</c></htm>
          </td></tr>
          <tr><td>
            <htm><js>...</js></htm> <b>=&gt;</b> <htm><c language="javascript">...</c></htm>
          </td></tr>
          <tr><td>
            <htm><sql>...</sql></htm> <b>=&gt;</b> <htm><c language="sql">...</c></htm>
          </td></tr>
          <tr><td>
            <htm><css>...</css></htm> <b>=&gt;</b> <htm><c language="css">...</c></htm>
          </td></tr>
        </table>
        <br>
        <table width="100%" border=1>
          <tr><td style="background-color:#E0E0E0;"><i>Code blocks highlighting (abbreviations)</i></td>
          </tr>
          <tr><td>
            <htm><htm+code>...</htm+code></htm><br>
            <htm><code+htm>...</code+htm></htm><b> =&gt;</b> <htm><code language="html">...</code></htm>
          </td></tr>
          <tr><td>
            <htm><php+code>...</php+code></htm><br>
            <htm><code+php>...</code+php></htm> <b>=&gt;</b> <htm><code language="php">...</code></htm>
          </td></tr>
          <tr><td>
            <htm><js+code>...</js+code></htm><br>
            <htm><code+js>...</code+js></htm> <b>=&gt;</b> <htm><code language="javascript">...</code></htm>
          </td></tr>
          <tr><td>
            <htm><sql+code>...</sql+code></htm><br>
            <htm><code+sql>...</code+sql></htm> <b>=&gt;</b> <htm><code language="sql">...</code></htm>
          </td></tr>
          <tr><td>
            <htm><css+code>...</css+code></htm><br>
            <htm><code+css>...</code+css></htm> <b>=&gt;</b> <htm><code language="css">...</code></htm>
          </td></tr>
        </table>
        <br>
        <table border=1>
          <tr><td colspan=2 style="background-color:#E0E0E0;"><i>Plaintext highlighting</i></td>
          </tr>
          <tr>
            <td valign=top width="1%">
              <span class="phtml Tag">&lt;ltt></span>...<span class="phtml Tag">&lt;/ltt></span>
            </td>
            <td>
            List of TTs, according to the language given in the surrounding <htm><hilite>...</hilite></htm> tag,
            with highlighting of <tt>$</tt>-prefixed language variables (PHP only).
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><vars>...</vars></htm>
            </td>
            <td>
            Plaintext highlighting of language variables (PHP only).
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><predefs>...</predefs></htm>&nbsp;&nbsp;
            </td>
            <td>
            Plaintext highlighting of predefined symbols (i.e., integer, mixed, string ...).
            </td>
          </tr>
          <tr>
            <td valign=top>
              <htm><names>...</names></htm>
            </td>
            <td>
            Same as:<br>
            <htm><vars><predefs>...</predefs></vars></htm>
            </td>
          </tr>
        </table>
        <br>
        <table border=1>
          <tr><td colspan=2 style="background-color:#E0E0E0;"><i>Root tags</i></td>
          </tr>
          <tr>
            <td valign=top width="1%">
              <span class="phtml Tag">&lt;hilite</span> <span class="phtml Attr">language</span><span class="phtml Tag">=</span><span class="phtml AttrVAl">"$LANG [vars|predefs|names]"</span><span class="phtml Tag">></span>...<span class="phtml Tag">&lt;/hilite></span>
              <br><br>
              The surrounding root tag that is needed to perform plaintext highlighting according
              to the rules of the language <span class="phtml AttrVal">$LANG</span>, optionally
              including the <htm><vars>...</vars></htm>, <htm><predefs>...</predefs></htm>,
              or <htm><names>...</names></htm> highlighting as a default.
            </td>
          </tr>
          <tr>
            <td valign=top width="1%">
              <span class="phtml Tag">&lt;script</span> <span class="phtml Attr">language</span><span class="phtml Tag">=</span><span class="phtml AttrVAl">"inactive $LANG"</span><span class="phtml Tag">></span>...<span class="phtml Tag">&lt;/script></span><br>
              where <span class="phtml Attrval">$LANG</span> = <tt>(html|php|javascript|sql|css)</tt>.
              <br><br>
              Same as with <htm><code>...</code></htm>, except that here we are using <htm><script>...</script></htm>,
              which is a CDATA tag.<br>
              <br>
              Using <htm><script>...</script></htm> is necessary to highlight, e.g. markup containing tags
              like <span class="phtml Tag">&lt;head></span>, <span class="phtml Tag">&lt;body></span>,
              etc., or to prevent the browser from modifying the highlighted markup beforehand (for example,
              adding <span class="phtml Tag">&lt;tbody></span> tags inside a
              <span class="phtml Tag">&lt;table></span>...<span class="phtml Tag">&lt;/table></span>).
            </td>
          </tr>
        </table>

        </hilite>
        <br><br>

        <font size=0><br></font>

        <a name="AppendixB"/>
        <span style="font-size: 32px; background-color:#E0E0E0;"><i>Appendix B</i>: CSS reference</span><br>
        <br>
        The following CSS classes should be defined according to the colors &amp; styles the user wants
        to see in her highlighted output (see <tt>styles/hilite.css</tt> for an example). Overall, we
        have the following classes:
<ul style="margin-top:0;">
<li>
  <i><tt>Comment</tt></i>: for comments ;
<li>
  <i><tt>Operator</tt></i>: for operators (scripting &amp; programming languages only) ;
<li>
  <i><tt>Keyword</tt></i>: for keywords (scripting &amp; programming languages only) ;
<li>
  <i><tt>Predef</tt></i>: for predefined symbols (scripting &amp; programming languages only) ;
<li>
  <i><tt>ConstString</tt></i>: for string constants (scripting &amp; programming languages only) ;
<li>
  <i><tt>Symbol</tt></i>: for symbols (scripting &amp; programming languages only) ;
<li>
  <i><tt>Variable</tt></i>: for variables (scripting &amp; programming languages, when the variables can be lexically identified, e.g. with the <tt>$</tt> prefix in PHP) ;
</ul>

Plus:
<ul style="margin-top:0;">
<li>
  <i><tt>ListOfTT</tt></i>: (scripting languages, plaintext only: styles of the elements in an <span class="phtml Tag">&lt;ltt></span>...<span class="phtml Tag">&lt;/ltt></span> tag) ;
<li>
  <i><tt>xmlPI</tt></i>: XML processing instruction (HTML only) ;
<li>
  <i><tt>CommentContent</tt></i>: Comment content (HTML only) ;
<li>
  <i><tt>Tag</tt></i>: HTML tag name (HTML only) ;
<li>
  <i><tt>Attr</tt></i>: HTML attribute name (HTML only) ;
<li>
  <i><tt>AttrVal</tt></i>: HTML attribute value (HTML only) ;
<li>
  <i><tt>Text</tt></i>: HTML text (HTML only) ;
</ul>

<hilite>
Those are either existing as independent CSS classes, or can be combined with CSS class
names related to the current language ; there are 2 kinds, one for defining the highlighting
used in <htm><script>...</script></htm> tags, and another for defining the highlighting used
in plaintext-enabled highlighting (i.e. inside an <htm><hilite>...</hilite></htm> tag). The language-related
CSS classes are named according to the language (e.g., "<tt>php</tt>"), and the corresponding classes
for plaintext-enabled highlighting is prefixed by "p" (e.g, "<tt>pphp</tt>").
</hilite>
        <br><br>

        <!--<span style="font-size: 32px; background-color:#E0E0E0;"><i>Appendix C</i>: Validating your markup</span><br>
        <br>
        To <i>validate</i> your markup, you can use the command <tt>hilite</tt> on the command line (which
        is to be found in the directory <tt>bin/</tt> of your current <tt>hilite.js</tt> installation),
        along with the <tt>-validate</tt> option and the name of the folder where your HTML files reside.
        <br><br>
        In the following example, showing the validator detecting a missing closing <span class="phtml Tag">&lt;/table></span>
        tag, the <tt>hilite.js</tt> package is located in <tt>/home/henri/hilite</tt>, and the HTML
        files of our current project are located in <tt>/home/henri/doc</tt> ; we obtain:<br>
        <table border=1><tr><td style="font-size:20px; background-color:#E0E0E0;">
        <tt>
        /home/henri>hilite/bin/hilite -validate doc<br>
        Missing closing tag for &lt;td&gt; --&gt; .//doc/manual.html, line 513, col 19<br>
        /home/henri>_
        </tt>
        </td></tr></table>
        <br>
        After having fixed the error, we run again the validator, and then we get:<br>
        <table border=1><tr><td style="font-size:20px; background-color:#E0E0E0;">
        <tt>
        /home/henri>hilite/bin/hilite -validate doc<br>
        Validating manual.html<br>
        Validating about.html<br>
        Validating examples.html<br>
        Validating contact.html<br>
        /home/henri>_
        </tt>
        </td></tr></table>
        <br>
        All the HTML files in the directory <tt>/home/henri/doc</tt> have now been validated.
        <br><br>
        
        <br>-->

        <span style="font-size: 32px; background-color:#E0E0E0;"><i>Appendix <!--D-->C</i>: Building <!--and extending--> <tt>hilite.js</tt></span><br>
        <br>
        The system comes with a precompiled version of the <tt>hilite.js</tt> library located in
        the <tt>bin/</tt> directory of your current installation.
        <br><br>
        The <i>source code</i> of the system is located in the <tt>src/</tt> directory of your current installation
        of the <tt>hilite.js</tt> system. This directory contains a dozen of Javascript modules, that are assembled
        into the <tt>bin/hilite.js</tt> library  by means of a (very simple) build script (this build script is
        triggered by <tt>make</tt>, and is located in <tt>bin/build.sh</tt>).
        <br><br>
        All in all, the source code of the system can thus be modified in a quite convenient way, and the
        <tt>hilite.js</tt> library that you use in your pages very easily rebuilt, too.
        <br><br>
        <!--Moreover, the <tt>src/</tt> directory also contains files having the <tt>.lang</tt> extension, which
        describe the properties pertaining to the highlighting process that <tt>hilite.js</tt> performs. For
        each language, there is a corresponding <tt>.lang</tt> file, for example, the file <tt>src/php.lang</tt>
        describes the PHP language.
        <br><br>
        In <tt>hilite.js</tt>, the <i>programming languages</i> are considered as being made of <i>tokens</i>,
        which can belong to the following categories:
<ul style="margin-top:0;">
<li>
  <i>Operators</i>, which are given under the section <tt>op</tt> of a given <tt>.lang</tt> file ;
<li>
  <i>Keywords</i>, which are given under the section <tt>keyw</tt> of a given <tt>.lang</tt> file. The
  keywords can be case-<tt>sensitive</tt> or case-<tt>insensitive</tt> (as a default, they are case-sensitive) ;
<li>
  <i>Predefined symbols</i>, which are given under the section <tt>pred</tt> of a given <tt>.lang</tt> file. The
  predefined symbols can be case-<tt>sensitive</tt> or case-<tt>insensitive</tt> (as a default, they are case-sensitive) ;
<li>
  <i>Symbols</i>, <i>Constants</i>, <i>Variables</i> and <i>Comments</i> are detected by the tokenizer, and thus
  cannot be customized by the user writing a new <tt>.lang</tt> file. In fact, many programming languages share
  similar lexical definitions for tokens, which alleviates this problem ; in the future, we plan to extend the
  possibilities offered in the <tt>hilite.js</tt>'s <tt>.lang</tt> files to enable the description of more exotic
  langages ;
</ul>
        

        Finally, the HTML <i>markup language</i> (described in <tt>src/html.lang</tt>) is fully parsed, and we rather
        consider that an HTML snippet is made of recursively embedded <i>tags</i> (rather than tokens), which
        can belong to the following categories:
<ul style="margin-top:0;">
<li>
  <i>Self-closing tags</i>, which are given under the section <tt>selftag</tt> of the <tt>html.lang</tt> file ;
<li>
  <i>Auto-closing tags</i>, which are given under the section <tt>optclose</tt> of the <tt>html.lang</tt> file ;
<li>
  <i>CDATA tags</i>, which are given under the section <tt>cdata</tt> of the <tt>html.lang</tt> file ;
<li>
  <i>HTML comments</i> and <i>XML processing instructions</i> are directly detected by the parser, and thus
  cannot be customized by the user ;
</ul>-->

        When you rebuild the system by means of <tt>make</tt> (or by means of <tt>bin/build.sh</tt>), <!--the
        <tt>.lang</tt> files are translated to Javascript datastructures, and--> all the Javascript modules
        are then assembled into <tt>bin/hilite.js</tt>.
        <br><br>
        <tt>hilite.js</tt> being a Javascript library meant to be modifiable by its users, there is no
        formal installation procedure in the underlying system <i>per se</i>. Rather, the advanced users
        who want to modify <tt>hilite.js</tt> are expected to work directly in the directory containing
        their own copy of the <tt>hilite.js</tt>'s source code.
        <br><br>
        Finally, the complete directory layout of the <tt>hilite.js</tt> system is as follows:
        <table border=1>
          <tr>
            <td valign=top><tt>/</tt></td>
            <td>contains the <tt>configure</tt> script, the <tt>Makefile</tt>, and the license
            </td>
          </tr>
          <tr>
            <td valign=top><tt>bin/</tt></td>
            <td>contains the generated programs and the scripts,
                mainly <tt>hilite.js</tt>, <!--the <tt>hilite</tt> command, and-->
                <tt>build.sh</tt>, <tt>clean.sh</tt>, and <tt>release.sh</tt>
            </td>
          </tr>
          <tr>
            <td valign=top><tt>styles/</tt></td>
            <td>contains a standard example stylesheet for styling your pages
            </td>
          </tr>
          <tr>
            <td valign=top><tt>src/</tt></td>
            <td>contains the source code of all the Javascript modules<!--, along with the language description files-->
            </td>
          </tr>
          <tr>
            <td valign=top><tt>web/</tt></td>
            <td>contains the documentation of the system
            </td>
          </tr>
        </table>
      </td>
      <td valign=top style="padding: 60px 0px 0px 0px; color:#0000EE;">
        <a href="about.html">About</a><br>
        <a href="download.html">Download</a><br>
        <a href="manual.html"><i>Manual</i></a><br>
        <a href="examples.html">Examples</a><br>
        <a href="contact.html">Contact</a><br>
      </td>
    </tr>
    </table>
    <br><br>
  </body>
</html>
