#
# Makefile
#
# Copyright (C) 2017  Sebastian Heimpel and Henri Lesourd
#
#  This file is part of hilite.js.
#
#  hilite.js is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  hilite.js is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
#

default: all

clean:
	@bin/clean.sh

all:
	@bin/build.sh

redo_release:
	@touch release

release: redo_release
	@bin/release.sh

dist: release

help:
	@echo "make clean"
	@echo "     all"
	@echo "     dist|release"
