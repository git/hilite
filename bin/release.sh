#!/bin/bash

#
# release.sh
#
# Copyright (C) 2017  Sebastian Heimpel and Henri Lesourd
#
#  This file is part of hilite.js.
#
#  hilite.js is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  hilite.js is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
#

HILITE_INSTALL=`dirname $0`/..
BIN=$HILITE_INSTALL/bin
SRC=$HILITE_INSTALL/src
STYLES=$HILITE_INSTALL/styles
WEB=$HILITE_INSTALL/web
RELEASE=$HILITE_INSTALL/release

CMD="rm -fr $RELEASE"
echo $CMD
eval $CMD

CMD="mkdir $RELEASE"
echo $CMD
eval $CMD

CMD="cp $HILITE_INSTALL/configure $RELEASE/configure"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/Makefile $RELEASE/Makefile"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/README $RELEASE/"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/INSTALL $RELEASE/INSTALL"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/COPYING $RELEASE/COPYING"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/NEWS $RELEASE/NEWS"
echo $CMD
$CMD

CMD="cp $HILITE_INSTALL/ChangeLog $RELEASE/ChangeLog"
echo $CMD
$CMD

CMD="mkdir $RELEASE/bin"
echo $CMD
$CMD

CMD="cp $BIN/build.sh $RELEASE/bin/build.sh"
echo $CMD
$CMD

CMD="cp $BIN/clean.sh $RELEASE/bin/clean.sh"
echo $CMD
$CMD

CMD="cp $BIN/release.sh $RELEASE/bin/release.sh"
echo $CMD
$CMD

CMD="cp $BIN/hilite.js $RELEASE/bin/hilite.js"
echo $CMD
$CMD

CMD="cp $BIN/hilite.js $WEB/hilite.js"
echo $CMD
$CMD

#CMD="cp $BIN/hilite_srv.js $RELEASE/bin/hilite_srv.js"
#echo $CMD
#$CMD

CMD="cp $STYLES/hilite.css $WEB/hilite.css"
echo $CMD
$CMD

CMD="cp $SRC/hilite.js $WEB/hilite.js"
echo $CMD
$CMD

CMD="mkdir $RELEASE/styles"
echo $CMD
$CMD

CMD="cp $STYLES/hilite.css $RELEASE/styles/hilite.css"
echo $CMD
$CMD

CMD="mkdir $RELEASE/src"
echo $CMD
$CMD

CMD="cp $SRC/* $RELEASE/src/"
echo $CMD
$CMD

CMD="mkdir $RELEASE/web"
echo $CMD
$CMD

CMD="cp $WEB/* $RELEASE/web/"
echo $CMD
$CMD

CMD="rm -fr $RELEASE/web/*tgz"
$CMD

if [[ "$1" != "0" ]]; then
  cd $HILITE_INSTALL

  CMD="mv release hilite"
  eval "$CMD"

  CMD="tar -c hilite > release.tar"
  echo $CMD
  eval "$CMD"

  CMD="mv hilite release"
  eval "$CMD"

  CMD="gzip release.tar"
  echo $CMD
  $CMD

  CMD="mv release.tar.gz $WEB/hilite.tgz"
  echo $CMD
  $CMD
fi
