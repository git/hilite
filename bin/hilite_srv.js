/*
 * hilite_srv.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Includes
var fs=require('fs');
var path=require('path');

/*
 * defs.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Important definitions
var hilite_ready=false;
function hilite_init() { // The various characters and their categories
// TODO: put the appropriate splitters depending on each mode, e.g. right now, I'm not sure &nbsp; is recognized correctly in HTML
// FIXME: hilite_init() should become tokenize_init(), or something like that.
  if (!hilite_ready) {
    for (i=0;i<=255;i++) charnat[i]=UNKNOWN;
    for (i=asc("A");i<=asc("Z");i++) charnat[i]=ALPHANUM;
    for (i=asc("a");i<=asc("z");i++) charnat[i]=ALPHANUM;
    for (i=asc("0");i<=asc("9");i++) charnat[i]=ALPHANUM;
    charnat[228/*ae*/]=ALPHANUM; // TODO: add all the other accented characters (e.g. french, norse, etc.)
    charnat[246/*oe*/]=ALPHANUM;
    charnat[252/*ue*/]=ALPHANUM;
    charnat[asc("$")]=ALPHANUM;
    charnat[asc("?")]=ALPHANUM;
    charnat[asc("_")]=ALPHANUM;
    charnat[asc("@")]=ALPHANUM;
    charnat[asc("!")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    charnat[asc("-")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    //charnat[asc("&")]=ALPHANUM; // NOTE: Becomes a splitter when we tokenize js/php
    charnat[asc("#")]=SERVER?OPCHAR:ALPHANUM;
    charnat[asc("&")]=SPLITTER; // FIXME: Inside <code>...</code>, etc. (i.e., all the things that contain code (php code, javascript code, etc.) when used not inside <script>...</script>), like e.g., "=>" are turned to e.g. "&amp;&gt;"
    charnat[asc("+")]=SPLITTER;
    charnat[asc("*")]=SPLITTER;
    charnat[asc("%")]=SPLITTER;
    charnat[asc(",")]=SPLITTER;
    charnat[asc(":")]=SPLITTER;
    charnat[asc(";")]=SPLITTER;
    charnat[asc(".")]=SPLITTER;
    charnat[asc("=")]=SPLITTER;
    charnat[asc("|")]=SPLITTER;
    charnat[asc("(")]=charnat[asc(")")]=SPLITTER;
    charnat[asc("[")]=charnat[asc("]")]=SPLITTER;
    charnat[asc("{")]=charnat[asc("}")]=SPLITTER;    
    charnat[asc("<")]=charnat[asc(">")]=charnat[asc("/")]=SPLITTER;
    charnat[asc("\\")]=ALPHANUM;
    charnat[asc("\n")]=BLANK;
    charnat[asc("\r")]=BLANK;
    charnat[asc(" ")]=BLANK;
    charnat[asc("'")]=QUOTE;
    charnat[asc("\"")]=DQUOTE;
  }
  hilite_ready=true;
}

// This file has been generated from the .lang files in your src directory. Don't modify it directly.
// css
var cssop={ "{":1, "}":1, ";":1, "(":1, ")":1, ":":1, ",":1 };
var csskeyw={ "":1 };
var csspred={ "alignment-adjust":1, "alignment-baseline":1, "animation":1, "animation-delay":1, "animation-direction	":1, "animation-duration":1, "animation-iteration-count":1, "animation-name":1, "animation-play-state":1, "animation-timing-function":1, "appearance	":1, "azimuth":1, "background":1, "background-attachment":1, "background-break":1, "background-clip":1, "background-color":1, "background-image":1, "background-origin":1, "background-position":1, "background-repeat":1, "background-size":1, "baseline-shift":1, "binding":1, "bookmark-label":1, "bookmark-level":1, "bookmark-target":1, "border":1, "border-bottom":1, "border-bottom-color":1, "border-bottom-left-radius":1, "border-bottom-right-radius":1, "border-bottom-style":1, "border-bottom-width":1, "border-break":1, "border-collapse":1, "border-color":1, "border-image":1, "border-left":1, "border-left-color":1, "border-left-style":1, "border-left-width":1, "border-length":1, "border-radius":1, "border-right":1, "border-right-color":1, "border-right-style":1, "border-right-width":1, "border-spacing":1, "border-style":1, "border-top":1, "border-top-color":1, "border-top-left-radius":1, "border-top-right-radius":1, "border-top-style":1, "border-top-width":1, "border-width":1, "bottom":1, "box-align":1, "box-direction	":1, "box-flex":1, "box-flex-group":1, "box-lines":1, "box-ordinal-group":1, "box-orient":1, "box-pack":1, "box-shadow":1, "box-sizing":1, "caption-side":1, "clear":1, "clip":1, "color":1, "color-profile":1, "column-break-after":1, "column-break-before":1, "column-count":1, "column-fill":1, "column-gap	":1, "column-rule":1, "column-rule-color":1, "column-rule-style":1, "column-rule-width":1, "column-span":1, "column-width":1, "columns":1, "content	":1, "counter-increment":1, "counter-reset":1, "crop":1, "cue":1, "cue-after":1, "cue-before":1, "cursor":1, "direction":1, "display":1, "display-model":1, "display-role":1, "dominant-baseline":1, "drop-initial-after-adjust":1, "drop-initial-after-align":1, "drop-initial-before-adjust":1, "drop-initial-before-align":1, "drop-initial-size":1, "drop-initial-value":1, "elevation":1, "empty-cells":1, "fit":1, "fit-position":1, "float":1, "float-offset":1, "font":1, "font-family":1, "font-size":1, "font-size-adjust":1, "font-stretch	":1, "font-style	":1, "font-variant	":1, "font-weight	":1, "grid-columns":1, "grid-rows":1, "hanging-punctuation":1, "height":1, "hyphenate-after":1, "hyphenate-before":1, "hyphenate-character":1, "hyphenate-lines":1, "hyphenate-resource":1, "hyphens":1, "icon":1, "image-orientation":1, "image-resolution":1, "inline-box-align":1, "left":1, "letter-spacing	":1, "line-height	":1, "line-stacking":1, "line-stacking-ruby":1, "line-stacking-shift":1, "line-stacking-strategy":1, "list-style":1, "list-style-image":1, "list-style-position":1, "list-style-type":1, "margin":1, "margin-bottom":1, "margin-left":1, "margin-right":1, "margin-top":1, "mark":1, "mark-after":1, "mark-before":1, "marks":1, "marquee-direction":1, "marquee-play-count":1, "marquee-speed	":1, "marquee-style":1, "max-height":1, "max-width":1, "min-height":1, "min-width":1, "move-to	":1, "nav-down":1, "nav-index":1, "nav-left":1, "nav-right":1, "nav-up":1, "opacity":1, "orphans":1, "outline":1, "outline-color":1, "outline-offset":1, "outline-style":1, "outline-width":1, "overflow":1, "overflow-style":1, "overflow-x":1, "overflow-y":1, "padding":1, "padding-bottom":1, "padding-left":1, "padding-right":1, "padding-top":1, "page":1, "page-break-after":1, "page-break-before":1, "page-break-inside":1, "page-policy":1, "pause":1, "pause-after":1, "pause-before":1, "phonemes":1, "pitch":1, "pitch-range":1, "play-during":1, "position":1, "presentation-level":1, "punctuation-trim":1, "quotes":1, "rendering-intent":1, "resize":1, "rest":1, "rest-after":1, "rest-before":1, "richness":1, "right":1, "rotation":1, "rotation-point":1, "ruby-align":1, "ruby-overhang":1, "ruby-position":1, "ruby-span":1, "size":1, "speak	":1, "speak-header":1, "speak-numeral":1, "speak-punctuation":1, "speech-rate":1, "stress":1, "string-set":1, "tab-side":1, "table-layout":1, "target":1, "target-name":1, "target-new":1, "target-position":1, "text-align":1, "text-align-last":1, "text-decoration":1, "text-emphasis":1, "text-height":1, "text-indent":1, "text-justify":1, "text-outline":1, "text-replace":1, "text-shadow":1, "text-transform":1, "text-wrap	":1, "top":1, "transition":1, "transition-delay":1, "transition-duration":1, "transition-property":1, "transition-timing-function":1, "unicode-bidi":1, "vertical-align":1, "visibility":1, "voice-balance":1, "voice-duration":1, "voice-family":1, "voice-pitch":1, "voice-pitch-range":1, "voice-rate":1, "voice-stress":1, "voice-volume":1, "volume":1, "white-space	":1, "white-space-collapse":1, "widows":1, "width":1, "word-break":1, "word-spacing":1, "word-wrap":1, "z-index":1 };

// html
var selftag={ "link":1, "br":1, "hr":1, "img":1, "input":1 };
var optclose={ "li":1 };
var cdata={ "!DOCTYPE":1, "!--":1, "script":1, "?php":1, "!--?php":1 };

// js
var jsop={ "(":1, ")":1, "=":1, "<":1, ">":1, "{":1, "}":1, "[":1, "]":1, ",":1, ";":1 };
var jskeyw={ "var":1, "function":1, "for":1, "while":1, "return":1 };
var jspred={ "integer":1 };

// php
var phpop={ "(":1, ")":1, "=":1, "<":1, ">":1, "{":1, "}":1, "[":1, "]":1, ",":1, ";":1 };
var phpkeyw={ "__halt_compiler":1, "abstract":1, "and":1, "as":1, "break":1, "callable":1, "case":1, "catch":1, "class":1, "clone":1, "const":1, "continue":1, "declare":1, "default":1, "die":1, "do":1, "echo":1, "else":1, "elseif":1, "empty":1, "enddeclare":1, "endfor":1, "endforeach":1, "endif":1, "endswitch":1, "endwhile":1, "eval":1, "exit":1, "extends":1, "final":1, "for":1, "foreach":1, "function":1, "global":1, "goto":1, "if":1, "implements":1, "include":1, "include_once":1, "instanceof":1, "insteadof":1, "interface":1, "isset":1, "list":1, "namespace":1, "new":1, "or":1, "print":1, "private":1, "protected":1, "public":1, "require":1, "require_once":1, "return":1, "static":1, "switch":1, "throw":1, "trait":1, "try":1, "unset":1, "use":1, "var":1, "while":1, "xor":1 };
var phppred={ "integer":1, "string":1, "array":1, "mixed":1 };

// sql
var sqlop={ "(":1, ")":1, ",":1, ";":1, "=":1, "*":1 };
var sqlkeyw={ "a":1, "abort":1, "abs":1, "absolute":1, "access":1, "action":1, "ada":1, "add":1, "admin":1, "after":1, "aggregate":1, "alias":1, "all":1, "allocate":1, "also":1, "alter":1, "always":1, "analyse":1, "analyze":1, "and":1, "are":1, "as":1, "asc":1, "asensitive":1, "assertion":1, "assignment":1, "asymmetric":1, "at":1, "atomic":1, "attribute":1, "attributes":1, "audit":1, "authorization":1, "auto_increment":1, "avg":1, "avg_row_length":1, "backup":1, "backward":1, "before":1, "begin":1, "bernoulli":1, "between":1, "bit_length":1, "both":1, "breadth":1, "break":1, "browse":1, "bulk":1, "by":1, "c":1, "cache":1, "call":1, "called":1, "cardinality":1, "cascade":1, "cascaded":1, "case":1, "cast":1, "catalog":1, "catalog_name":1, "ceil":1, "ceiling":1, "chain":1, "change":1, "char_length":1, "character_length":1, "character_set_catalog":1, "character_set_name":1, "character_set_schema":1, "characteristics":1, "characters":1, "check":1, "checked":1, "checkpoint":1, "checksum":1, "class":1, "class_origin":1, "clob":1, "close":1, "cluster":1, "clustered":1, "coalesce":1, "cobol":1, "collate":1, "collation":1, "collation_catalog":1, "collation_name":1, "collation_schema":1, "collect":1, "column":1, "column_name":1, "columns":1, "command_function":1, "command_function_code":1, "comment":1, "commit":1, "committed":1, "completion":1, "compress":1, "compute":1, "condition":1, "condition_number":1, "connect":1, "connection":1, "connection_name":1, "constraint":1, "constraint_catalog":1, "constraint_name":1, "constraint_schema":1, "constraints":1, "constructor":1, "contains":1, "containstable":1, "continue":1, "conversion":1, "convert":1, "copy":1, "corr":1, "corresponding":1, "count":1, "covar_pop":1, "covar_samp":1, "create":1, "createdb":1, "createrole":1, "createuser":1, "cross":1, "csv":1, "cube":1, "cume_dist":1, "current":1, "current_date":1, "current_default_transform_group":1, "current_path":1, "current_role":1, "current_time":1, "current_timestamp":1, "current_transform_group_for_type":1, "current_user":1, "cursor":1, "cursor_name":1, "cycle":1, "data":1, "database":1, "databases":1, "datetime_interval_code":1, "datetime_interval_precision":1, "day_hour":1, "day_microsecond":1, "day_minute":1, "day_second":1, "dayofmonth":1, "dayofweek":1, "dayofyear":1, "dbcc":1, "deallocate":1, "dec":1, "decimal":1, "declare":1, "default":1, "defaults":1, "deferrable":1, "deferred":1, "defined":1, "definer":1, "degree":1, "delay_key_write":1, "delayed":1, "delete":1, "delimiter":1, "delimiters":1, "dense_rank":1, "deny":1, "depth":1, "deref":1, "derived":1, "desc":1, "describe":1, "descriptor":1, "destroy":1, "destructor":1, "deterministic":1, "diagnostics":1, "dictionary":1, "disable":1, "disconnect":1, "disk":1, "dispatch":1, "distinct":1, "distinctrow":1, "distributed":1, "div":1, "do":1, "domain":1, "drop":1, "dual":1, "dummy":1, "dump":1, "dynamic":1, "dynamic_function":1, "dynamic_function_code":1, "each":1, "element":1, "else":1, "elseif":1, "enable":1, "enclosed":1, "encoding":1, "encrypted":1, "end":1, "end-exec":1, "enum":1, "equals":1, "errlvl":1, "escape":1, "escaped":1, "every":1, "except":1, "exception":1, "exclude":1, "excluding":1, "exclusive":1, "exec":1, "execute":1, "existing":1, "exists":1, "exit":1, "exp":1, "explain":1, "external":1, "extract":1, "false":1, "fetch":1, "fields":1, "fillfactor":1, "filter":1, "final":1, "first":1, "float":1, "float4":1, "float8":1, "floor":1, "flush":1, "following":1, "for":1, "force":1, "foreign":1, "fortran":1, "forward":1, "found":1, "free":1, "freetext":1, "freetexttable":1, "freeze":1, "from":1, "full":1, "fulltext":1, "function":1, "fusion":1, "g":1, "general":1, "generated":1, "get":1, "global":1, "go":1, "goto":1, "grant":1, "granted":1, "grants":1, "greatest":1, "group":1, "grouping":1, "handler":1, "having":1, "header":1, "heap":1, "hierarchy":1, "high_priority":1, "hold":1, "holdlock":1, "host":1, "hosts":1, "hour_microsecond":1, "hour_minute":1, "hour_second":1, "identified":1, "identity":1, "identity_insert":1, "identitycol":1, "if":1, "ignore":1, "ilike":1, "immediate":1, "immutable":1, "implementation":1, "implicit":1, "in":1, "include":1, "including":1, "increment":1, "index":1, "indicator":1, "infile":1, "infix":1, "inherit":1, "inherits":1, "initial":1, "initialize":1, "initially":1, "inner":1, "inout":1, "input":1, "insensitive":1, "insert":1, "insert_id":1, "instance":1, "instantiable":1, "instead":1, "intersect":1, "intersection":1, "interval":1, "into":1, "invoker":1, "is":1, "isam":1, "isnull":1, "isolation":1, "iterate":1, "join":1, "k":1, "key":1, "key_member":1, "key_type":1, "keys":1, "kill":1, "lancompiler":1, "language":1, "large":1, "last":1, "last_insert_id":1, "lateral":1, "leading":1, "least":1, "leave":1, "left":1, "length":1, "less":1, "level":1, "like":1, "limit":1, "lineno":1, "lines":1, "listen":1, "ln":1, "load":1, "local":1, "localtime":1, "localtimestamp":1, "location":1, "locator":1, "lock":1, "login":1, "logs":1, "loop":1, "low_priority":1, "lower":1, "m":1, "map":1, "match":1, "matched":1, "max":1, "max_rows":1, "maxextents":1, "maxvalue":1, "mediumblob":1, "mediumint":1, "mediumtext":1, "member":1, "merge":1, "message_length":1, "message_octet_length":1, "message_text":1, "method":1, "middleint":1, "min":1, "min_rows":1, "minus":1, "minute_microsecond":1, "minute_second":1, "minvalue":1, "mlslabel":1, "mod":1, "mode":1, "modifies":1, "modify":1, "module":1, "month":1, "monthname":1, "more":1, "move":1, "multiset":1, "mumps":1, "myisam":1, "name":1, "names":1, "national":1, "natural":1, "nchar":1, "nclob":1, "nesting":1, "new":1, "next":1, "no":1, "no_write_to_binlog":1, "noaudit":1, "nocheck":1, "nocompress":1, "nocreatedb":1, "nocreaterole":1, "nocreateuser":1, "noinherit":1, "nologin":1, "nonclustered":1, "none":1, "normalize":1, "normalized":1, "nosuperuser":1, "not":1, "nothing":1, "notify":1, "null":1, "notnull":1, "nowait":1, "nullif":1, "nulls":1, "octet_length":1, "of":1, "off":1, "offline":1, "offset":1, "offsets":1, "oids":1, "old":1, "on":1, "online":1, "only":1, "open":1, "opendatasource":1, "openquery":1, "openrowset":1, "openxml":1, "operation":1, "operator":1, "optimize":1, "option":1, "optionally":1, "options":1, "or":1, "order":1, "ordering":1, "ordinality":1, "others":1, "out":1, "outer":1, "outfile":1, "output":1, "over":1, "overlaps":1, "overlay":1, "overriding":1, "owner":1, "pack_keys":1, "pad":1, "parameter":1, "parameter_mode":1, "parameter_name":1, "parameter_ordinal_position":1, "parameter_specific_catalog":1, "parameter_specific_name":1, "parameter_specific_schema":1, "parameters":1, "partial":1, "partition":1, "pascal":1, "password":1, "path":1, "pctfree":1, "percent":1, "percent_rank":1, "percentile_cont":1, "percentile_disc":1, "placing":1, "plan":1, "pli":1, "position":1, "postfix":1, "power":1, "preceding":1, "precision":1, "prefix":1, "preorder":1, "prepare":1, "prepared":1, "preserve":1, "primary":1, "print":1, "prior":1, "privileges":1, "proc":1, "procedural":1, "procedure":1, "process":1, "processlist":1, "public":1, "purge":1, "quote":1, "raid0":1, "raiserror":1, "range":1, "rank":1, "raw":1, "read":1, "reads":1, "readtext":1, "real":1, "recheck":1, "reconfigure":1, "recursive":1, "ref":1, "references":1, "referencing":1, "regexp":1, "regr_avgx":1, "regr_avgy":1, "regr_count":1, "regr_intercept":1, "regr_r2":1, "regr_slope":1, "regr_sxx":1, "regr_sxy":1, "regr_syy":1, "reindex":1, "relative":1, "release":1, "reload":1, "rename":1, "repeat":1, "repeatable":1, "replace":1, "replication":1, "require":1, "reset":1, "resignal":1, "resource":1, "restart":1, "restore":1, "restrict":1, "result":1, "return":1, "returned_cardinality":1, "returned_length":1, "returned_octet_length":1, "returned_sqlstate":1, "returns":1, "revoke":1, "right":1, "rlike":1, "role":1, "rollback":1, "rollup":1, "routine":1, "routine_catalog":1, "routine_name":1, "routine_schema":1, "row":1, "row_count":1, "row_number":1, "rowcount":1, "rowguidcol":1, "rowid":1, "rownum":1, "rows":1, "rule":1, "save":1, "savepoint":1, "scale":1, "schema":1, "schema_name":1, "schemas":1, "scope":1, "scope_catalog":1, "scope_name":1, "scope_schema":1, "scroll":1, "search":1, "second":1, "second_microsecond":1, "section":1, "security":1, "select":1, "self":1, "sensitive":1, "separator":1, "sequence":1, "serializable":1, "server_name":1, "session":1, "session_user":1, "set":1, "setof":1, "sets":1, "setuser":1, "share":1, "show":1, "shutdown":1, "signal":1, "similar":1, "simple":1, "size":1, "smallint":1, "some":1, "soname":1, "source":1, "space":1, "spatial":1, "specific":1, "specific_name":1, "specifictype":1, "sql":1, "sql_big_result":1, "sql_big_selects":1, "sql_big_tables":1, "sql_calc_found_rows":1, "sql_log_off":1, "sql_log_update":1, "sql_low_priority_updates":1, "sql_select_limit":1, "sql_small_result":1, "sql_warnings":1, "sqlca":1, "sqlcode":1, "sqlerror":1, "sqlexception":1, "sqlstate":1, "sqlwarning":1, "sqrt":1, "ssl":1, "stable":1, "start":1, "starting":1, "state":1, "statement":1, "static":1, "statistics":1, "status":1, "stddev_pop":1, "stddev_samp":1, "stdin":1, "stdout":1, "storage":1, "straight_join":1, "strict":1, "string":1, "structure":1, "style":1, "subclass_origin":1, "sublist":1, "submultiset":1, "substring":1, "successful":1, "sum":1, "superuser":1, "symmetric":1, "synonym":1, "sysdate":1, "sysid":1, "system":1, "system_user":1, "table":1, "table_name":1, "tables":1, "tablesample":1, "tablespace":1, "temp":1, "template":1, "temporary":1, "terminate":1, "terminated":1, "textsize":1, "than":1, "then":1, "ties":1, "time":1, "timestamp":1, "timezone_hour":1, "timezone_minute":1, "tinytext":1, "to":1, "toast":1, "top":1, "top_level_count":1, "trailing":1, "tran":1, "transaction":1, "transaction_active":1, "transactions_committed":1, "transactions_rolled_back":1, "transform":1, "transforms":1, "translate":1, "translation":1, "treat":1, "trigger":1, "trigger_catalog":1, "trigger_name":1, "trigger_schema":1, "trim":1, "true":1, "truncate":1, "trusted":1, "tsequal":1, "type":1, "uescape":1, "uid":1, "unbounded":1, "uncommitted":1, "under":1, "undo":1, "unencrypted":1, "unique":1, "unknown":1, "unlisten":1, "unlock":1, "unnamed":1, "unnest":1, "unsigned":1, "until":1, "update":1, "updatetext":1, "upper":1, "usage":1, "use":1, "user":1, "user_defined_type_catalog":1, "user_defined_type_code":1, "user_defined_type_name":1, "user_defined_type_schema":1, "using":1, "utc_date":1, "utc_time":1, "utc_timestamp":1, "vacuum":1, "valid":1, "validate":1, "validator":1, "value":1, "values":1, "var_pop":1, "var_samp":1, "variable":1, "variables":1, "verbose":1, "view":1, "volatile":1, "waitfor":1, "when":1, "whenever":1, "where":1, "while":1, "width_bucket":1, "window":1, "with":1, "within":1, "without":1, "work":1, "write":1, "writetext":1, "x509":1, "xor":1, "year_month":1, "zerofill":1, "zone":1 };
var sqlpred={ "array":1, "bit":1, "any":1, "integer":1, "bitvar":1, "bool":1, "boolean":1, "bigint":1, "binary":1, "blob":1, "char":1, "character":1, "date":1, "datetime":1, "day":1, "hour":1, "double":1, "file":1, "int":1, "int1":1, "int2":1, "int3":1, "int4":1, "int8":1, "integer":1, "long":1, "longblob":1, "longtext":1, "minute":1, "number":1, "nullable":1, "numeric":1, "object":1, "octets":1, "tinyblob":1, "tinyint":1, "text":1, "union":1, "varbinary":1, "varchar":1, "varchar2":1, "varcharacter":1, "varying":1, "year":1 };

// lang
var lang={
  "html":{ "style": { "code":"html", "plain":"phtml" } },
  "css":{ "style":
    { "code":"css", "plain":"pcss" },
    "op":cssop, "keyw":csskeyw, "pred":csspred, "keyw_sensitive":true, "pred_sensitive":true },
  "js":{ "style":
    { "code":"js", "plain":"pjs" },
    "op":jsop, "keyw":jskeyw, "pred":jspred, "keyw_sensitive":true, "pred_sensitive":true },
  "php":{ "style":
    { "code":"php", "plain":"pphp" },
    "op":phpop, "keyw":phpkeyw, "pred":phppred, "keyw_sensitive":false, "pred_sensitive":true },
  "sql":{ "style":
    { "code":"sql", "plain":"psql" },
    "op":sqlop, "keyw":sqlkeyw, "pred":sqlpred, "keyw_sensitive":false, "pred_sensitive":false }
};
lang["javascript"]=lang["js"];

/*
 * util.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Positions in files
function filePos(fname,li,col) {
  return {"fname":fname,"li":li,"col":col};
}
function filePosDup(fpos) {
  return filePos(fpos.fname,fpos.li,fpos.col);
}
function filePosLiCol(fpos,li,col) {
  fpos.li=li;
  fpos.col=col;
}
function filePosSerialize(fpos) {
  var res="";
  if (fpos.fname!=null) res+=fpos.fname+", ";
  res+="line "+fpos.li+", col "+fpos.col;
  return res;
}

// Error (filePos)
var _errorFilePos=null;
function errorPos() {
  if (_errorFilePos==null) return null;
  return filePosDup(_errorFilePos);
}
function errorFileName() {
  if (_errorFilePos==null) return null;
  return _errorFilePos.fname;
}
function errorLi() {
  if (_errorFilePos==null) return 0;
  return _errorFilePos.li;
}
function errorCol() {
  if (_errorFilePos==null) return 0;
  return _errorFilePos.col;
}
function errorSetPos(fname,li,col) {
  _errorFilePos=filePos(fname,li,col);
}
function errorSetLiCol(li,col) {
  if (_errorFilePos==null) return;
  filePosLiCol(_errorFilePos,li,col);
}
function errorResetPos() {
  _errorFilePos=null;
}

// Error
var SERVER=false;
function stop(errno) {
  if (SERVER) process.exit(errno); else zboc(1); // To stop it
}
function error(msg) {
  if (_errorFilePos!=null) msg+=" --> "+filePosSerialize(_errorFilePos);
  (SERVER?console.log:alert)(msg);
  stop(1);
}

// Useful constants
function ftrue() { return true; }
function ffalse() { return false; }
function ftrue1(a,b) { return true; }
function ffalse1(a,b) { return false; }
function ftrue2(a,b) { return true; }
function ffalse2(a,b) { return false; }

// Objects
function _proto(X) {
  return X.constructor.prototype;
}

function isNull(o) { return o==null; }
function isBoolean(o) { return o!=null && _proto(o)==_proto(true); }
function isNumber(o) { return o!=null && _proto(o)==_proto(1); }
function isString(o) { return o!=null && _proto(o)==_proto("A"); }
function isArray(o) { return o!=null && _proto(o)==_proto([]); }
function isDictionary(o) { return o!=null && _proto(o)==_proto({}); }
function isFunction(X) { return _proto(X)==_proto(function() {}); }

// Console & displays
function out(s) {
  document.write(s);
}
function br(s) {
  out("<br>");
}
function hr(s) {
  out("<hr>");
}

function stringEscape(s) {
  var res="";
  for (var i=0;i<s.length;i++) {
    var c=s[i];
    if (c=='"') c="\\\"";
    if (c=='\n') c="\\n";
    res+=c;
  }
  return res;
}
var displaying=[],display_mode="cooked";
function displayMode(mode) {
  display_mode=mode;
}
function display(o) { // Similar to JSON.stringify()
  var res=null;
  if (isNull(o)) res="null";
  if (isBoolean(o)) res=o.toString();
  if (isNumber(o)) res=o.toString();
  if (isString(o)) {
  /*if (display_mode=="raw")*/ res=o;
    if (display_mode=="cooked") res='"'+o+'"';
    if (display_mode=="html") res=htmlEscapeChars(stringEscape(o));
  }
  if (contains(displaying,o)) res="^^";
  else
  if (isArray(o)) {
    displaying.push(o);
    res="[";
    for (var i=0;i<o.length;i++) {
      if (i>0) res+="|";
      res+=display(o[i]);
    }
    res+="]";
    displaying.pop();
  }
  else
  if (isDictionary(o)) {
    displaying.push(o);
    res="{";
    var first=true;
    for (var val in o) {
      if (!first) res+="|"; else first=false;
      if (val!="parent") {
        res+=val+"="+display(o[val]);
      }
    }
    res+="}";
    displaying.pop();
  }
  return res;
}

// Characters
function asc(ch) {
  return ch.charCodeAt(0)&255;
}
function chr(i) {
  return String.fromCharCode(i);
}

// Strings
function trim(s,chars,left,right) {
  if (chars==undefined) chars=" ";
  if (left==undefined) left=true;
  if (right==undefined) right=true;
  var res="",a=s.split(""),i;
  if (left) {
    i=0;
    while (i<a.length && chars.indexOf(a[i])!=-1) {
      a[i]=null;
      i++;
    }
  }
  if (right) {
    i=a.length-1;
    while (i>=0 && chars.indexOf(a[i])!=-1) {
      a[i]=null;
      i--;
    }
  }
  for (i=0;i<a.length;i++) if (a[i]!=null) res+=a[i];
  return res;
}
function startsWith(s,pref) {
  if (s.length<pref.length) return false;
  else {
    return s.substring(0,pref.length)==pref;
  }
}
function endsWith(s,suff) {
  if (s.length<suff.length) return false;
  else {
    return s.substring(s.length-suff.length,s.length)==suff;
  }
}
function stringFind(s,ss) {
  for (var i=0;i<s.length;i++) {
    if (startsWith(s.substring(i,s.length),ss)) return i;
  }
  return -1;
}
function splitTrim(s,chars) {
  var a=s.split(chars);
  for (var i=0;i<a.length;i++) a[i]=trim(a[i]," ",true,true);
  return a;
}
function replaceAll(s,s1,s2) {
  var s0;
  do {
    s0=s;
    s=s.replace(s1,s2);
  }
  while (s!=s0);
  return s;
}
function lcase(s) {
  return s.toLowerCase();
}
function ucase(s) {
  return s.toUpperCase();
}

// Arrays
function length(o) {
  if (isString(o) || isArray(o)) return o.length;
  if (isDictionary(o)) return Object.getOwnPropertyNames(o).length;
  return undefined;
}
function empty(st) {
  return length(st)==0;
}
function last(st) { // TODO: Shit, top(), is already defined. Find a way to have it, or another decent name
  if (empty(st)) return null;
  return st[st.length-1];
}
function contains(a,o) {
  if (isString(a)) return stringFind(a,o)!=-1;
  else
  if (isArray(a)) {
    for (var i=0;i<a.length;i++) if (a[i]===o) return true;
  }
  else error("contains");
  return false;
}
function index(a,o) {
  for (var i=0;i<a.length;i++) if (a[i]===o) return i;
  return -1;
}
function arrayToDict(a) {
  var d={};
  for (var i=0;i<a.length;i++) if (a[i]!=null) d[a[i][0]]=a[i][1];
  return d;
}
function splice(t,i,ndel,t2) {
  t.splice.apply(t,[i,ndel].concat(t2));
}
function arrayCopy(a,i0,j0) {
  var res=[];
  for (var i=i0;i<j0;i++) res.push(a[i]);
  return res;
}
function arrayTrim(a,left,right) {
  var i0=0,j0=length(a);
  if (left)
    while (i0<length(a) && isString(a[i0]) && length(a[i0])>0 && charIs(a[i0][0],BLANK)) i0++;
  if (right)
    while (j0>0 && isString(a[j0-1]) && length(a[j0-1])>0 && charIs(a[j0-1][0],BLANK)) j0--;
  return arrayCopy(a,i0,j0);
}

// Argv
function argvRemove(argv,j) {
  splice(argv,j,1,[]);
}

/*
 * tokenizer.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Tokenizer
var UNKNOWN=0,ALPHANUM=1,OPCHAR=2,SPLITTER=3,BLANK=4,QUOTE=5,DQUOTE=6;
var charnat=[];

function charIs(ch,nat) {
  return charnat[asc(ch)]==nat;
}
function charIsDigit(ch) {
  return ch=="1" || ch=="2" || ch=="3" || ch=="4" || ch=="5"
      || ch=="6" || ch=="7" || ch=="8" || ch=="9" || ch=="0";
}

var buf,ptr;
function bufEof() {
  return ptr>=buf.length;
}

var _ptrNextCount=true;
function ptrNext() {
  var li=errorLi(),col=errorCol();
  if (buf[ptr]=='\n') li++,col=1; else col++;
  if (_ptrNextCount) errorSetLiCol(li,col);
  ptr++;
}
function ptrNextN(n) {
  while (n--) ptrNext();
}
function find(nat) {
  while (ptr<buf.length && !charIs(buf[ptr],nat)) ptrNext();
}
function findChar(ch) {
  while (ptr<buf.length && buf[ptr]!=ch) ptrNext();
}
function findString(str) {
  while (ptr<buf.length+str.length-1 && buf.substring(ptr,ptr+str.length)!=str) ptrNext();
  if (ptr>=buf.length+str.length-1) ptr=buf.length; // FIXME: Doesn't do the appropriate calls to ptrNext()
}
function skip(nat) {
  while (ptr<buf.length && charIs(buf[ptr],nat)) ptrNext();
}
function isHtmlEscapedChar() {
  var i=ptr,res=false;
  if (buf[i]=="&") {
    i++;
    while (i<buf.length && charIs(buf[i],ALPHANUM)) i++;
    if (i<buf.length && buf[i]==";") res=true;
  }
  return res;
}
function next(skipblanks,ignoreSQuote/*FIXME: Hack*/) {
  if (skipblanks) skip(BLANK);
  var res=null;
  if (ptr<buf.length) {
    var ptr0=ptr;
    if (buf.substring(ptr,ptr+2)=="/*") {  // TODO: Finish Javascript & PHP comments
      var li=errorLi(),col=errorCol();
      findString("*/");
      if (ptr>=buf.length) errorSetLiCol(li,col),error("Unterminated comment");
      ptrNextN(2);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (buf.substring(ptr,ptr+2)=="//") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (!SERVER/*FIXME: Hack*/ && buf.substring(ptr,ptr+1)=="#") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (!SERVER/*FIXME: Hack*/ && scriptLang=="sql" && buf.substring(ptr,ptr+2)=="--") {
      var li=errorLi(),col=errorCol();
      findString("\n");
      if (ptr<buf.length) ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (buf.substring(ptr,ptr+1)=="&" && isHtmlEscapedChar()) {
      var li=errorLi(),col=errorCol();
      findString(";");
      ptrNext();
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],SPLITTER)) res=buf[ptr],ptrNext();
    else
    if (charIs(buf[ptr],BLANK)) {
      skip(BLANK);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],QUOTE) && ignoreSQuote) res=buf[ptr],ptrNext();
    else
    if (charIs(buf[ptr],ALPHANUM)) {
      skip(ALPHANUM);
      res=buf.substring(ptr0,ptr);
    }
    else
    if (charIs(buf[ptr],OPCHAR)) {
      skip(OPCHAR);
      res=buf.substring(ptr0,ptr);
    }
    else {
      var li=errorLi(),col=errorCol();
      for (var q=QUOTE;q<=DQUOTE;q++) if (ptr<buf.length && charIs(buf[ptr],q)) {
        ptrNext();
        find(q);
        if (ptr>=buf.length) errorSetLiCol(li,col),error("Unterminated string");
        ptrNext();
        res=buf.substring(ptr0,ptr);
      }
      if (res==null) errorSetLiCol(li,col),error("Unknown element: "+buf[ptr]+";;"+charnat[asc(buf[ptr])]);
    }
  }
//alert(res);
  return res;
}
function tokenize() {
  var s,res=[];
  do {
    s=next(false,false);
    if (s!=null) res.push(s);
  }
  while (s!=null);
  return res;
}
function bufstart(txt) {
  buf=txt;
  ptr=0;
}

// Tokenizing scripts
function scriptTokenize(code) {
  var oldbuf=buf,oldptr=ptr,oldfpos=errorPos();
  bufstart(code);
  var s,res=[];
  do {
    s=next(false,false);
    if (s!=null) res.push(s);
  }
  while (s!=null);
  buf=oldbuf;ptr=oldptr;
  if (oldfpos!=null) errorSetPos(oldfpos.fname,oldfpos.li,oldfpos.col);
  return res;
}

/*
 * parser.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Trees
function tagCreate(state,tag,elts,li,col) {
  if (li==undefined) li=-1;
  if (col==undefined) col=-1;
  return {"state":state,"parent":null,"tag":tag,"attrs":{},"elts":elts,"licol":[li,col]};
}
function isXmlPI(tag) {
  return isDictionary(tag) && tag.state=="?" && !isHtmlComment(tag);
}
function isHtmlComment(tag) {
  return isDictionary(tag) && tag.state=="?" && tag.tag=="!--";
}
function isOpeningTag(tag) {
  return isDictionary(tag) && tag.state=="<";
}
function isClosingTag(tag) {
  return isDictionary(tag) && tag.state==">";
}
function isCDataTag(tag) {
  return isDictionary(tag) && cdata[tag.tag];
}
function isOptCloseTag(tag) {
  return isDictionary(tag) && optclose[tag.tag];
}
function isSelfClosingTag(tag) {
  return isDictionary(tag) && tag.state=="</";
}
function isSelfTag(tag) { // FIXME: Messy ...
  return !isClosingTag(tag) && ((isDictionary(tag) && selftag[tag.tag]) || isCDataTag(tag) || isSelfClosingTag(tag));
}
function isClosedTag(tag) {
  return isDictionary(tag) && tag.state=="<>";
}
function isTag(tag,name,attr,fval) {
  if (!isDictionary(tag) || !(tag.state=="<>" || tag.state=="<") || tag.tag!=name) return false;
  return fval(attrVal(tag.attrs[attr]));
}
function tagAdd(tag,elt) {
  tag.elts.push(elt);
  if (isDictionary(elt)) elt.parent=tag;
}
function tagReplace(dest,src) {
  if (!isDictionary(dest) || !isDictionary(src)) error("tagReplace");
  dest.state=src.state;
  dest.tag=src.tag;
  dest.attrs=src.attrs;
  dest.elts=src.elts;
}
function treeCopy(p,t) {
  var res=t;
  if (isString(t)) ;
  else
  if (isDictionary(t)) {
    res={"state":t.state,"parent":p,"tag":t.tag,"attrs":{},"elts":[]};
    for (var n in t.attrs) res.attrs[n]=t.attrs[n];
    for (var i=0;i<t.elts.length;i++) res.elts.push(treeCopy(res,t.elts[i]));
  }
  else error("treeCopy");
  return res;
}

// Traversals
var _foreach_elt_modified;
function foreach_elt_modified() {
  return _foreach_elt_modified;
}
var _foreach_cut=false;
function foreach_cut() {
  _foreach_cut=true;
}
function foreach_elt_bis(node,func) {
  if (isDictionary(node)) {
    if (node.tag!="!--") {
      var onode=node;
      node=func(node);
      if (node!=onode) _foreach_elt_modified=true;
      if (!_foreach_cut) {
        var elt=node.elts;
        for (var i=0;i<elt.length;i++) {
          elt[i]=foreach_elt_bis(elt[i],func);
        }
      }
    }
    _foreach_cut=false;
  }
  return node;
}
function foreach_elt(node,func) {
  _foreach_elt_modified=false;
  _foreach_cut=false;
  return foreach_elt_bis(node,func);
}
function foreach_text_bis(node,attrs,func) {
  var res=node;
  if (isDictionary(node)) {
    var elt=node.elts;
    for (var i=0;i<elt.length;i++) {
      elt[i]=foreach_text_bis(elt[i],node,func);
      if (_foreach_cut) break;
    }
    _foreach_cut=false;
  }
  else
  if (isString(node)) res=func(node,attrs);
  return res;
}
function foreach_text(node,func) {
  _foreach_cut=false;
  return foreach_text_bis(node,node,func);
}

// Parsing & serializing HTML
function cdataNext(end,end2) { // FIXME: Skip comments and strings, in case they would contain end
  var res=null,ptr0=ptr,s,s2;
  var first=true;
  do {
    findChar(end[0]);
    s=s2="";
    if (!bufEof() && (s=buf.substring(ptr,ptr+end.length))!=end
                  && (s2=buf.substring(ptr,ptr+end2.length))!=end2) ptrNext();
  }
  while (!bufEof() && s!=end && s2!=end2);
  if (!bufEof()) res=buf.substring(ptr0,ptr);
  return res;
}
function tagReadAttrs(tag) {
  str=next(true,false);
  while (str!="/" && str!=">") {
    var name=str;
    var li=errorLi(),col=errorCol();
    str=next(true,false);
    if (str!="=") errorSetLiCol(li,col),error("Attribute expected");
    str=next(true,false);
    tag.attrs[name]=str;
    str=next(true,false);
  }
  if (str=="/") {
    tag.state="</";
    var li=errorLi(),col=errorCol();
    str=next(true,false);
    if (str!=">") errorSetLiCol(li,col),error("'>' expected [<tag ... />]");
  }
}
function htmlNext() {
  if (ptr>=buf.length) return null;
  var li=errorLi(),col=errorCol(),
      str=next(false,true);
  if (str!="<") res=str;
  else 
  tag: {
    var opn=charnat[asc("+")]; // FIXME: Hack
    charnat[asc("+")]=ALPHANUM;
    str=next(true,false);
    charnat[asc("+")]=opn;
    if (str=="/") {
      opn=charnat[asc("+")]; // FIXME: Hack (2)
      charnat[asc("+")]=ALPHANUM;
      str=next(true,false);
      charnat[asc("+")]=opn;
      res=tagCreate(">",str,[],li,col);
      li=errorLi(),col=errorCol();
      str=next(true,false);
      if (str!=">") errorSetLiCol(li,col),error("'>' expected [</tag>]");
    }
    else
    if (cdata[str] || str=="code") {
      res=tagCreate("?",str,"",li,col);
      var end="",end2=chr(255);
      if (str=="!--?php") { // Mangled PHP
        end="?-->";res.tag="?php";
      }
      else
      if (str[0]=="?") end="?>"; // e.g. <?php ... ?>
      else
      if (str=="!--") {  // HTML comments
        end="-->";res.tag="!--";
      }
      else
      if (str[0]=="!") end=">"; // e.g. <!DOCTYPE ...>
      else { // e.g. <script language="javascript">
        res.state="<>";
        end="</"+str+">";
        end2="<//"+str+">";
        tagReadAttrs(res);
        if (isTag(res,"script","language",
            function(lang) { return isString(lang) && contains(lang,"html"); }))
        {
          res.state="<";res.elts=[];
          break tag;
        }
      }
      var ptr0=ptr;
      str=cdataNext(end,end2);
      if (str==null && end=="?-->") {
        ptr=ptr0; // FIXME: Doesn't count the #lines correctly, with this
        str=cdataNext(end="?&gt;",""); // Repair the fucking mangling that the FUCKING browsers do with <?php
        if (str!=null) {
          var i=str.indexOf("-->");
          str=str.substring(0,i)+str.substring(i+2,str.length);
        }
      }
      if (str==null) errorSetLiCol(li,col),error("'"+end+"' expected");
      res.elts=str;
      if (buf.substring(ptr,ptr+end.length)==end) ptrNextN(end.length);
      if (buf.substring(ptr,ptr+end2.length)==end2) ptrNextN(end2.length);
    }
    else {
      res=tagCreate("<",str,[],li,col);
      tagReadAttrs(res);
    }
  }
//alert("res="+display(res));
  return res;
}
function htmlTokenize() {
  var s,res=[];
  do {
    s=htmlNext();
    if (s!=null) res.push(s);
  }
  while (s!=null);
  return res;
}

function htmlParse(fpos) {
  if (fpos!=undefined) errorSetPos(fpos.fname,fpos.li,fpos.col);
  var elt,res=[tagCreate("<>","",[])];
  var finished=false;
  var finalize=function() {
  //if (!SERVER/*FIXME: Horrid hack (1)*/) if (empty(res) || elt!=null && last(res).tag==elt.tag) return;
    var closed=res.pop();
    closed.state="<>";
    if (empty(res)) {
      res=closed;
      finished=true;
    }
    else tagAdd(last(res),closed);
  };
  do {
    var li=errorLi(),col=errorCol();
    elt=htmlNext();
    if (elt!=null) {
      var scripth=isTag(elt,"script","language",
                        function(lang) { return isString(lang) && contains(lang,"html"); });
      if (!scripth && (isCDataTag(elt) && !isHtmlComment(elt) || elt.tag=="code"/*FIXME: Hack*/)) {
        elt.elts=scriptTokenize(elt.elts);
      }
      if (!scripth && (isString(elt) || isSelfTag(elt) || elt.tag=="code"/*FIXME: Hack*/)) {
        tagAdd(last(res),elt);
      }
      else
      if (isOpeningTag(elt)) res.push(elt);
      else
      if (isClosingTag(elt)) {
        var closed=null;
        while (last(res).tag!=elt.tag) { // FIXME: Should detect closing tags without the corresponding opening tag
          closed=res.pop();
          if (closed.state=="<" && !isOptCloseTag(closed)) {
            errorSetLiCol(closed.licol[0],closed.licol[1]);
            error("Missing closing tag for <"+closed.tag+">");
          }
          if (last(res)==null) errorSetLiCol(li,col),error("Lone closing tag"); //alert(display(elt.tag));
          tagAdd(last(res),closed);
        }
        finalize();
      }
      else errorSetLiCol(li,col),error("Unknown case"); // Not reached
    }
  //else if (!SERVER/*FIXME: Horrid hack (2)*/) finalize();
  }
  while (elt!=null && !finished);
  if (htmlNext()!=null) error("Markup found past end");
  errorResetPos();
  return res[0];
}

function htmlDisplay(o) {
  displaying=[o.parent];
  var res=display(o);
  displaying=[];
  return res;
}
function scriptRaw(code) {
  var res="";
  for (var i=0;i<code.length;i++) {
    res+=code[i];
  }
  return res;
}
function htmlSerializePlainText(html,prewrap) {
  return htmlSerialize(html,true,prewrap);
}
function htmlSerialize(html,plain/*FIXME: Find a fix for this horrid hack*/,prewrap) {
  if (plain==undefined) plain=false;
  var res="";
  if (plain && isDictionary(html) && (html.tag=="?php" || html.tag=="script")/*here we have to check the attributes*/) {
    res=htmlPretty(html);
  }
  else
  if (isDictionary(html)) {
    if (html.tag!="") {
      res="<"+html.tag;
      var attrs=html.attrs;
      if (!empty(attrs)) {
        for (var n in attrs) {
          if (n[0]!="$"/* FIXME: Hack*/) res+=" "+n+"="+attrs[n];
        }
      }
      if (!isHtmlComment(html) && !isXmlPI(html)) {
        res+=(isSelfClosingTag(html)?"/":"")+">";
      }
    }
    var elt=html.elts;
    if (html.tag=="!--") res+=elt;
    else
    if (html.tag=="script"/*the remaining script tags*/) res+=scriptRaw(elt);
    else
    if (elt) {
      for (var i=0;i<length(elt);i++) {
        res+=htmlSerialize(elt[i],plain,prewrap);
      }
    }
    if (html.tag!="") {
      if (isClosedTag(html)) res+="</"+html.tag+">";
      if (isHtmlComment(html)) res+="-->";
      if (isXmlPI(html))
        if (html.tag[0]=="!"/*FIXME: crap hack for <!DOCTYPE ...>*/) res+=">"; else res+="?>";
    }
    if (!elt) res="";
  }
  else
  if (isString(html)) {
    if (prewrap && contains(html,"\n")) { html=replaceAll(html,"\n","<br>"); }
    res=html;
  }
  return res;
}

/*
 * pretty.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Pretty (HTML)
function htmlEscapeChars(html) {
  var res="";
  for (var i=0;i<html.length;i++) {
    var c=html[i];
    if (c=='<') c="&lt;";
    if (c=='>') c="&gt;";
    if (c=='&') c="&amp;";
    res+=c;
  }
  return res;
}
function htmlEscapeBlanks(html) {
  var res="";
  for (var i=0;i<html.length;i++) {
    var c=html[i];
    if (c=='\n') c="<br>";
    if (c==' ') c="<span style=\"white-space:pre-wrap;\"> </span>"; //"&nbsp;"; // TODO: Make the generated HTML code shorter (with an <spc> tag, styled with CSS)
    res+=c;
  }
  return res;
}
function htmlSpan(categ,txt) {
  return "<span class=\""+categ+"\">"+txt+"</span>";
}

function htmls(cla) { // FIXME: Hack
  return (scriptStyleName=="plain"?"p":"")+"html "+cla;
}
function attrVal(val) {
  if (val==undefined || val==null) val="";
  return trim(trim(val,"\"",true,true)," ",true,true);
}
function attrLang(lang) { // FIXME: Remove that, the parser should not know "inactive"
  lang=attrVal(lang);
  lang=trim(replaceAll(lang,"inactive","")," ",true,true);
  return lang;
}
function htmlPretty(html) {
  var res="";
  if (isDictionary(html)) {
    if (html.tag[0]=="\\" || length(html.tag)>1 && html.tag[1]=="\\") { // FIXME: Hack to be able to escape tags
      html.tag=replaceAll(html.tag,"\\","");
    }
    if (html.tag!="" && html.attrs["foldtag"]==undefined) {
      res=htmlSpan(isHtmlComment(html)?htmls("Comment"):isXmlPI(html)?htmls("xmlPI"):htmls("Tag"),
                   "&lt;"+html.tag);
      var attrs=html.attrs;
      if (!empty(attrs)) {
        for (var n in attrs) {
          if (n[0]!="$"/* FIXME: Hack*/) {
            var val=attrs[n];
            if (!SERVER/*FIXME: hack*/) val=htmlEscapeChars(val);
            res+="&nbsp;"+
                 htmlSpan(htmls("Attr"),n)+
                 htmlSpan(htmls("Tag"),"=")+
                 htmlSpan(htmls("AttrVal"),val);
          }
        }
      }
      if (!isHtmlComment(html) && !isXmlPI(html)) {
        res+=htmlSpan(htmls("Tag"),(isSelfClosingTag(html)?"/":"")+"&gt;");
      }
    }
    var elt=html.elts,
        lang=attrLang(html.attrs["language"]);
    if (html.tag=="!--") res+=htmlSpan(htmls("CommentContent"),elt);
    else
    if (html.tag=="?php") {
      enterScriptPretty("php",scriptStyleName);
      res+=scriptPretty(elt);
      leaveScriptPretty();
    }
    else
    if (html.tag=="script" && lang!="html") {
      if (lang=="") lang="javascript";
      if (!scriptLangExists(lang)) error("Unknown language: "+lang);
      enterScriptPretty(lang,scriptStyleName);
      res+=scriptPretty(elt);
      leaveScriptPretty();
    }
    else
    if (elt) {
      for (var i=0;i<elt.length;i++) {
        res+=htmlPretty(elt[i]);
      }
    }
    if (html.tag!="" && html.attrs["foldtag"]==undefined) {
      if (isClosedTag(html)) res+=htmlSpan(htmls("Tag"),"&lt;/"+html.tag+"&gt;");
      if (isHtmlComment(html)) res+=htmlSpan(htmls("Comment"),"--&gt;");
      if (isXmlPI(html))
        if (html.tag[0]=="!"/*FIXME: crap hack for <!DOCTYPE ...>*/)
          res+=htmlSpan(htmls("xmlPI"),"&gt;");
        else
          res+=htmlSpan(htmls("xmlPI"),"?&gt;");
    }
    if (!elt) res="";
  }
  else
  if (isString(html)) {
    var categ=charIs(html[0],BLANK)?null:htmls("Text"); // FIXME: Quite hacky smelling, somehow ...
    res=htmlEscapeBlanks(html);
    if (html[0]=='"' || html[0]=="'") res=htmlEscapeChars(html);
    if (categ) res=htmlSpan(htmls("Text"),res);
  }
  return res;
}

// Pretty (PHP)
var scriptLang="php",
    scriptStyleName="code",
    scriptStyle=lang[scriptLang]["style"][scriptStyleName]; // e.g.  php, pphp
var
   prettyStack=[];
function scriptLangExists(lan) {
  for (var l in lang) if (l==lan) return true;
  return false;
}
function enterScriptPretty(lan,sty) { // FIXME: Make it generic ;; TODO: Test here that the language actually exists, and then remove the tests about that everywhere else
  prettyStack.push([scriptLang,scriptStyleName]);
  scriptLang=lan;
  scriptStyleName=sty;
  scriptStyle=lang[lan]["style"][sty];
}
function leaveScriptPretty() {
  var lan=prettyStack.pop();
  scriptLang=lan[0];
  scriptStyleName=lan[1];
}
function isScriptVar(s) {
  if (scriptLang=="html") return false;
  return scriptLang=="php"?s[0]=='$':false;
}
function isScriptOp(s) {
  if (scriptLang=="html") return false;
  return lang[scriptLang].op[s.toLowerCase()];
}
function isScriptKeyw(s) {
  if (scriptLang=="html") return false;
  if (!lang[scriptLang].keyw_sensitive) s=s.toLowerCase();
  return lang[scriptLang].keyw[s];
}
function isScriptPred(s) {
  if (scriptLang=="html") return false;
  if (!lang[scriptLang].pred_sensitive) s=s.toLowerCase();
  return lang[scriptLang].pred[s]; // TODO: Verify that the PHP predefs are indeed f$%&/(g case /insensitive/ ...
}
function scriptPrettyVar(tok) {
  if (isScriptVar(tok)) return htmlSpan(scriptStyle+" Variable",tok);
  return tok;
}
function scriptPrettyPredef(tok) {
  if (isScriptPred(tok)) return htmlSpan(scriptStyle+" Predef",tok);
  return tok;
}
function scriptPrettyName(tok) {
  if (isScriptVar(tok)) return htmlSpan(scriptStyle+" Variable",tok);
  if (isScriptPred(tok)) return htmlSpan(scriptStyle+" Predef",tok);
  return tok;
}
function scriptMarkVars(str) {
  res="";
  for (var i=0;i<str.length;i++) {
    if (str[i]!="$" || scriptLang!="php") res+=str[i];
    else {
      var i0=i;
      while (charIs(str[i],ALPHANUM)) i++;
      res+=htmlSpan(scriptStyle+" Variable",str.substring(i0,i));
      i--;
    }
  }
  return res;
}
function scripts(cla) { // FIXME: Hack
  return scriptStyle+" "+cla;
}
function scriptReplaceEscapedSymbs(tok,htmls) {
  if (htmls) {
    tok=replaceAll(tok,"&lt;","<");
    tok=replaceAll(tok,"&gt;",">");
  }
  tok=replaceAll(tok,"\\\\","&zboc;");
  tok=replaceAll(tok,"&zboc;","\\");
  return tok;
}
function scriptPrettyElt(tok) {
  var categ=scripts("Symbol");
  tok=scriptReplaceEscapedSymbs(tok,true);
  if (charIs(tok[0],BLANK)) return htmlEscapeBlanks(tok);
  if (tok[0]=="'" || tok[0]=='"') { tok=scriptMarkVars(htmlEscapeBlanks(htmlEscapeChars(tok)));categ=scripts("ConstString"); }
  if (charIsDigit(tok[0])) { categ=scripts("ConstString"); } // TODO: should we use only "Const" ?
  if (tok.length>=2 && tok[0]=="/" && tok[1]=='*') categ=scripts("Comment");
  if (tok.length>=2 && tok[0]=="/" && tok[1]=='/') {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (tok.length>=1 && tok[0]=="#") {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (tok.length>=1 && tok[0]=="-" && tok[1]=="-" && scriptLang=="sql") {
    categ=scripts("Comment");
    tok=trim(tok,"\n",false,true);tok+="<br>";
  }
  if (isScriptOp(tok)) categ=scripts("Operator");
  if (isScriptKeyw(tok)) categ=scripts("Keyword");
  if (isScriptPred(tok)) categ=scripts("Predef");
  if (tok[0]=="$") categ=scripts("Variable");
  return htmlSpan(categ,tok);
}
function scriptPretty(code) {
  var res="";
  for (var i=0;i<code.length;i++) {
    res+=scriptPrettyElt(code[i]);
  }
  return res;
}

/*
 * transforms.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Rules
function ruleCreate(tag,test,func) {
  var res={ "tag":null, "test":null, "func":null,
            "frule":null };
  res.tag=tag;
  res.test=test;
  res.func=func;
  return res;
}
function frulesCreate(tr) {
  return { "rules":[], "transform":tr };
}

function ruleApply(r,t) {
  return r.func(t,t);
}

// Transforms
function transformCreate() {
  return { "tag":{}, "string":{}, "begblocks":[] };
}
function frulesAddRule(f,r) {
  f.rules.push(r);
  r.frules=f;
}
function transformAddRule(t,r) {
  if (isNull(t.tag[""])) t.tag[""]=frulesCreate(t);
  frulesAddRule(t.tag[""],ruleCreate("",ftrue2,r));
}

function transformApplyFlat(t,node) {
  var first=true;
  for (n in t.tag) {
    for (var i=0;i<t.tag[n].rules.length;i++) {
      node=ruleApply(t.tag[n].rules[i],node);
      if (_foreach_cut) break;
    }
    if (_foreach_cut) break;
  }
  return node;
}
function transformApply(t,node) {
  node=foreach_elt(node,function (elt) {
    return transformApplyFlat(t,elt);
  });
  return node;
}
function transform(node,t) {
  return transformApply(t,node);
}

/*
 * rules.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Rules
function ruleCreate() {
  return { "type":"string", "matcher":[], "vars":[], "pattern":[], "conds":[], "cvars":[],
           "tag":null, "test":null, "func":null,
           "frule":null };
}
function ruleTagCreate(tag,test,func) {
  var res=ruleCreate();
  res.type="tag";
  res.tag=tag;
  res.test=test;
  res.func=func;
  return res;
}
function frulesCreate(tr) {
  return { "rules":[], "transform":tr };
}

function stringIsVar(s) {
  return isString(s) && s[0]=="$";
}
var DEBUG_RULES=false;
function displayRuleMatch(v,r,elts) {
  var i1=r[0],i2=r[1];
  var res=v+" <::> [";
  var first=true;
  for (var i=i1;i<=i2;i++) res+=(first?"":",")+'"'+elts[i]+'"',first=false;
  res+="]";
  return res;
}
function ruleStringConstMatch(c,s) {
  if (!isString(s)) return false;
  if (c=="\\n") return contains(s,"\n") || contains(s,"\r");
           else return c==s;
}
function ruleStringMatchAtPos(r,elts,j0) {
  var m=r.matcher;
  var vars={};
  var i=0,j=j0;
  while (i<m.length) {
    if (isString(m[i])) {
      if (stringIsVar(m[i])) {
        if (i+1==m.length) {
          var j1=j;
          while (j<elts.length && !contains(r.frules.transform.begblocks,elts[j])) j++;
          vars[m[i]]=[j1,j-1];
          if (DEBUG_RULES) console.log(displayRuleMatch(m[i],vars[m[i]],elts));
        }
        else {
          var j1=j;
          while (j<elts.length && !ruleStringConstMatch(m[i+1],elts[j])) j++;
          if (j==elts.length) {
            if (DEBUG_RULES) console.log("Can't match "+m[i]);
            return false;
          }
          vars[m[i]]=[j1,j-1];
          if (DEBUG_RULES) console.log(displayRuleMatch(m[i],vars[m[i]],elts));
        }
      }
      else {
        if (!ruleStringConstMatch(m[i],elts[j])) {
          if (DEBUG_RULES) console.log("Can't match "+m[i]+" ["+elts[j]+"]");
          return false;
        }
        if (DEBUG_RULES) console.log("<::> "+m[i]);
        j++;
      }
    }
    else {
      var pat=m[i],tr=elts[j];
      if (!isDictionary(tr) || length(pat.elts)!=1 || !stringIsVar(pat.elts[0]) || pat.tag!=tr.tag)
      {
        if (DEBUG_RULES) console.log("Can't match "+htmlSerialize(pat)+"<::>"+htmlSerialize(tr));
        return false;
      }
      vars[pat.elts[0]]=[[j],0,tr.elts.length-1];
      j++;
    }
    i++;
  }
  vars["all"]=[j0,j-1];
  var cvars={};
  for (var i=0;i<r.cvars.length;i++) {
    cvars[r.cvars[i][0]]=r.cvars[i][1];
  }
  vars["cvars"]=cvars;
  for (var i=0;i<r.conds.length;i++) {
    var val=r.conds[i];
    val=stringReplaceVars(val,elts,vars);
    if (!evaljs(val)) {
      if (DEBUG_RULES) console.log("Can't satisfy condition "+val);
      return false;
    }
  }
  return vars;
}
function ruleStringMatch(r,t) {
  if (r.type!="string") error("ruleStringMatch");
  if (DEBUG_RULES) {
    console.log("Matching:\n  "+ruleDisplay(r)+"\n<::>\n"+htmlSerialize(t));
    console.log("-->");
  }
  var res=false;
  for (var i=0;i<t.elts.length;i++) {
    res=ruleStringMatchAtPos(r,t.elts,i);
    if (res) break;
  }
  return res;
}

function stringReplaceVar(s,v,val) {
//console.log("Replacing "+v+" in <<"+s+">> by "+val);
  var i=stringFind(s,v);
  if (i!=-1 && !(i>0 && s[i-1]=="\\")) {
    s=s.substring(0,i)+val+s.substring(i+v.length,s.length);
  }
//console.log("--> "+s);
  return s;
}
function treeSubtree(t,p) {
  for (var i=0;i<p.length;i++) t=t.elts[p[i]];
  return t;
}
function matcherVarVal(m,v,elts) {
  var r=m[v];
  if (r.length==2) return elts.slice(r[0],r[1]+1);
  else {
    var tr=treeSubtree({"elts":elts}/*FIXME:Crappy hack*/,r[0]);
    return tr.elts.slice(r[1],r[2]+1);
  }
}
var _transform_glob=null;
function stringReplaceGlobs(s) {
  while (contains(s,"$")) {
    var v=trim(s,"\"",true,true);
    v=trim(v,"$",true,false);
    if (_transform_glob!=null && _transform_glob[v]!=undefined) s=stringReplaceVar(s,"$"+v,_transform_glob[v]);
                                                           else error("Variable not found: $"+v);
  }
  return s;
}
function stringReplaceVars(s,elts,m) {
  if (contains(s,"$")) {
    for (var n in m) if (stringIsVar(n)) {
      var val=matcherVarVal(m,n,elts).map(function (x) { return htmlSerialize(x,false,false); }).join("");
      s=stringReplaceVar(s,n,val);
    }
  }
  if (contains(s,"$")) {
    if (_transform_glob!=null) {
      for (var n in _transform_glob) {
        var val=_transform_glob[n];
        s=stringReplaceVar(s,"$"+n,val);
      }
    }
  }
  //if (contains(s,"$")) error("Variable not found: $"+s);
  return s;
}
function evaljs(snip) {
  var cmd="";
  var instr=false,minstr=false;
  for (var i=0;i<snip.length;i++) {
    var c=snip[i];
    if (c=="{" && i+1<snip.length && snip[i+1]=='"') {
      i++;
      c=snip[i];
      instr=minstr=true;
    }
    else
    if (c=='"' && i+1<snip.length && snip[i+1]=='}') {
      i++;
      instr=minstr=false;
    }
    else {
      var oinstr=instr;
      if (!instr) {
        if (c=="'") instr=c;
        if (c=='"') instr=c;
      }
      else {
        if (c===instr) instr=false;
      }
      if ((instr || minstr) && c=="\n") c="\\n";
      if ((instr || minstr) && c=="\r") c="";
      if (oinstr==instr) {
        if (instr && c=='"') c='\\"';
        if (instr && c=="'") c="\\'";
      }
    }
    cmd+=c;
  }
//console.log(cmd);
  return eval(cmd);
}
function ruleReplaceVars(t,elts,m) {
  if (!isDictionary(t)) error("ruleReplaceVars");
  for (var n in m.cvars) { // FIXME: Crappy to redo that each time
    var val=m.cvars[n];
    val=stringReplaceVars(val,elts,m);
    m.cvars[n]=val;
  }
  for (var a in t.attrs) {
    var s=t.attrs[a];
    s=stringReplaceVars(s,elts,m);
    if (contains(s,"$")) {
      for (var n in m.cvars) {
        var val=evaljs(m.cvars[n]); // FIXME: Crappy to redo eval() each time
        s=stringReplaceVar(s,n,val);
      }
    }
    //if (contains(s,"$")) stringReplaceVars(s,elts,m);
    if (contains(s,"$")) s=trim(s,"\"",true,true),error("Variable not found: "+s);
    t.attrs[a]=s;
  }
  for (var i=0;i<t.elts.length;i++) {
    var e=t.elts[i];
    if (isDictionary(e)) ruleReplaceVars(t.elts[i],elts,m);
    if (stringIsVar(e)) {
      if (m[e]) splice(t.elts,i,1,matcherVarVal(m,e,elts));
      else
      if (m.cvars[e]) splice(t.elts,i,1,evaljs(m.cvars[e])); // FIXME: Crappy to redo eval() each time
    }
  }
}
function ruleReplaceMatch(r,t,m) {
  var t2=treeCopy(t,r.pattern[0]);
  ruleReplaceVars(t2,t.elts,m);
  splice(t.elts,m["all"][0],m["all"][1]-m["all"][0]+1,[t2]);
}
function ruleStringApply(r,t,all) {
  var m,res=false;
  do {
    m=ruleStringMatch(r,t);
  //console.log(display(m));
    if (m) {
      ruleReplaceMatch(r,t,m);
      res=true;
    }
    if (!all) break;
  }
  while (m);
  return res;
}
function ruleTagApply(r,t) { // Currently, a ruleTag's function is launched only once
  return r.func(t,t);
}

// Transforms
function transformCreate() {
  return { "tag":{}, "string":{}, "begblocks":[] };
}
function frulesAddRule(f,r) {
  f.rules.push(r);
  r.frules=f;
}
function transformAddRule(t,r) {
//console.log(ruleDisplay(r));
  if (isFunction(r)) {
    if (isNull(t.tag[""])) t.tag[""]=frulesCreate(t);
    frulesAddRule(t.tag[""],ruleTagCreate("",ftrue2,r));
  }
  else
  if (r.type=="tag") {
    if (isNull(t.tag[r.tag])) t.tag[r.tag]=frulesCreate(t);
    frulesAddRule(t.tag[r.tag],r);
  }
  else {
    var key=r.matcher[0];
    if (!isString(key)) {
      key="<"+key.tag+">";
    }
    if (isNull(t.string[key])) t.string[key]=frulesCreate(t);
    frulesAddRule(t.string[key],r);
    if (stringIsVar(r.matcher[length(r.matcher)-1])) t.begblocks.push(r.matcher[0]);
  }
}
function transformRead(fname) {
  var res=transformCreate();
  var dir=path.dirname(fname);
  var html=htmlFileRead(fname);
  var elt=html.elts;
  if (elt) {
    var i0=0;
    while (charIs(elt[i0][0],BLANK)) i0++;
    if (elt[i0]=="uses") { // FIXME: Make several
      i0++;
      while (charIs(elt[i0][0],BLANK)) i0++;
      var jsfname=trim(elt[i0],'"',true,true);
    //console.log("Loading "+jsfname);
      var js=fs.readFileSync(dir+"/"+jsfname,"ascii");
      var objs=eval(js);
      for (var n in objs) global[n]=objs[n];
      i0++;
    }
    var prev="\n",r=null,reading="pattern";
    for (var i=i0;i<elt.length;i++) {
      if (elt[i]==":" && i+1<elt.length && elt[i+1]==">")
      {
        reading="conds";
        i++;
      }
      else
      if (elt[i]=="=" && i+1<elt.length && elt[i+1]==">")
      {
        reading="pattern";
        i++;
      }
      else
      if (reading=="conds") {
        while (charIs(elt[i][0],BLANK) && !contains(elt[i],"\n")) i++;
        if (!contains(elt[i],"\n")) {
          var cond="";
          while (!contains(elt[i],"\n")) cond+=elt[i],i++;
          if (cond[0]=="$" && contains(cond,"=")) { /*FIXME: quick hack*/
            cond=cond.split("=");
            r.cvars.push(cond);
          }
          else r.conds.push(cond);
          i--;
        }
      }
      else {
        if (reading=="pattern" && isString(prev)
         && contains(prev,"\n") && endsWith(prev,"\n"))
        {
          if (r!=null) transformAddRule(res,r);
          r=ruleCreate();
          reading="matcher";
        }
        if (!(isString(elt[i]) && charIs(elt[i][0],BLANK))) {
          var val=elt[i];
          if (isString(val) && (val[0]=="'" || val[0]=='"')) val=val.substring(1,val.length-1);
          if (reading=="matcher") {
            if (val[0]=="$") r.vars.push(val);
            r.matcher.push(val);
          }
          else r.pattern.push(val);
        }
      }
      prev=elt[i];
    }
    transformAddRule(res,r);
  }
  return res;
}

function displayHtmlList(lst) {
  var res="";
  var first=true;
  for (var i=0;i<lst.length;i++) {
    res+=first?"":" ";
    if (isString(lst[i])) res+=lst[i];
                     else res+=htmlSerialize(lst[i]);
  }
  return res;
}
function ruleDisplay(r) {
  var res="";
  res+="["+displayHtmlList(r.matcher)+"](";
  var first=true;
  for (var i=0;i<r.vars.length;i++) res+=(first?"":",")+r.vars[i],first=false;
  res+=")=>["+htmlSerialize(tagCreate("<>","",r.pattern))+"]";
  var started=false;
  if (!empty(r.conds)) {
    res+="\n:>";started=true;
    for (var i=0;i<r.conds.length;i++) res+="\n  "+r.conds[i];
  }
  if (!empty(r.cvars)) {
    if (!started) res+="\n:>";
    for (var i=0;i<r.cvars.length;i++) res+="\n  "+r.cvars[i][0]+"="+r.cvars[i][1];
  }
  return res;
}
function transformDisplay(t) {
  var res=display(t.begblocks)+"\n",first=true;
  for (n in t.string) for (var i=0;i<t.string[n].rules.length;i++) {
    res+=(first?"":"\n")+ruleDisplay(t.string[n].rules[i]);
    first=false;
  }
  return res;
}

function transform_modified() {
  return foreach_elt_modified();
}
function transformApplyFlat(t,node) {
  var found=false,first=true;
  for (n in t.string) for (var i=0;i<t.string[n].rules.length;i++) {
  //console.log((first?"":"\n")+ruleDisplay(t.string[n].rules[i])),first=false;
    var b=ruleStringApply(t.string[n].rules[i],node,true);
    found=found || b;
  }
  for (n in t.tag) {
    for (var i=0;i<t.tag[n].rules.length;i++) {
      node=ruleTagApply(t.tag[n].rules[i],node);
      if (_foreach_cut) break;
    }
    if (_foreach_cut) break;
  }
  _foreach_elt_modified=_foreach_elt_modified || found;
  return node;
}
var TRANSFORM_SKIP_CODE=false; // FIXME: Crappy hack to exclude <*code*>...</*code*> tags from rules' application
function transformApply(t,node) {
  var found=false;
  node=foreach_elt(node,function (elt) {
    if (!(TRANSFORM_SKIP_CODE
       && (contains(elt.tag,"code") || elt.tag=="c"
        || elt.tag=="htm" || elt.tag=="js" || elt.tag=="php" || elt.tag=="sql" || elt.tag=="css")))
    { // FIXME: Reaaaallly crap (1) ...
      elt=transformApplyFlat(t,elt);
      found=found || transform_modified();
    }
    return elt;
  });
  _foreach_elt_modified=_foreach_elt_modified || found;
  return node;
}
function transformSetGlob(glob) {
  _transform_glob=glob;
}
function transform(node,t) {
  return transformApply(t,node);
  transformSetGlob(null);
}

/*
 * files.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Files
function fileExists(fname) {
  return fs.existsSync(fname);
}
function fileRead0(fname) {
  if (!fileExists(fname)) error("File not found: "+fname);
  return fs.readFileSync(fname,"ascii");
}
var htmlFileRead_CRSplitter=false;
function htmlFileRead(fname) {
  val=fileRead0(fname);
  val=replaceAll(val,"\r\n","\n");
  hilite_init();
  if (htmlFileRead_CRSplitter) charnat[asc("\n")]=SPLITTER; // FIXME: Crap hack for splitting blanks containing CRs in some cases
  val=trim(val,"\n",1,0);
  val=trim(val," \n",0,1);
  bufstart(val);
  val=htmlParse(filePos(fname,1,1));
  return val;
}
_READ={ "html":htmlFileRead };
function fileReadSet(ext,readFunc) {
  _READ[ext]=readFunc;
}
function fileExt(fname) {
  var a=fname.split(".");
  return a[length(a)-1];
}
function fileRead(fname) {
  var dir=path.dirname(fname)+"/";
  fname=path.basename(fname);
  var val="";
  var readFunc=_READ[fileExt(fname)];
  if (readFunc!=undefined) {
    val=readFunc(dir+fname);
  }
  else {
    val=fs.readFileSync(dir+fname,"ascii");
    val=replaceAll(val,"\r","");
    val=trim(val,"\n",1,0);
    val=trim(val," \n",0,1);
  }
  return val;
}
function fileWrite(fname,str) {
  try {
    fs.writeFileSync(fname,str);
  }
  catch (err) {
    error("Can't write file: "+fname);
  }
}
function fileDelete(fname) {
  fs.unlinkSync(fname);
}

// Directories
function dirCreate(fname) {
  try {
    fs.mkdirSync(fname);
  }
  catch (err) {
    error("Can't create directory: "+fname);
  }
}
function isDir(fname) {
  var fd=fs.openSync(fname,"r");
  var stats=fs.fstatSync(fd);
  return stats.isDirectory();
}
function vfileCreate(fname,isDir,parent,val) {
//console.log("Loading "+fname);
  return {"fname":fname,"isDir":isDir,isModified:false,"val":val, "parent":parent};
}
function dirRead(fname,mask) {
  var predir=path.dirname(fname)+"/";
  fname=path.basename(fname);
  var d=vfileCreate(fname,true,null,{});
  var dir=fname;
  if (predir!="") dir=predir+"/"+fname;
  var a=fs.readdirSync(dir);
  for (var i=0;i<a.length;i++) {
    var b=isDir(dir+"/"+a[i]);
    if (b) {
      d.val[a[i]]=dirRead(dir+"/"+a[i],mask);
      d.val[a[i]].parent=d;
    }
    else {
      d.val[a[i]]=vfileCreate(a[i],b,d,null);
      if (mask==undefined || endsWith(a[i],mask)) d.val[a[i]].val=fileRead(dir+"/"+a[i]);
    }
  }
  return d;
}
function foreach_vfile(d,func) {
  for (var fname in d.val) {
    var f=d.val[fname];
    if (f.isDir) foreach_vfile(f,func);
            else func(f);
  }
  func(d);
}
function vfilePathname(vf) {
  var a=[];
  do {
    a.push(vf.fname);
    vf=vf.parent;
  }
  while (vf!=null);
  return replaceAll(path.normalize(a.reverse().join("/")),"\\","/");
}
function isChildPath(p,parent) {
  p=path.normalize(p);
  parent=path.normalize(parent);
  if (parent[0]=="." && p[0]!='/') p="./"+p; //FIXME: Sort out all the other cases with "."
  return startsWith(p,parent);
}

/*
 * inherit_attrs.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Inheriting attributes
function foreach_collectAttrs(attrs,node) {
  if (!isDictionary(attrs) || !isDictionary(node)) error("foreach_collectAttrs");
  var collect=false,res=attrs;
  for (var n in node.attrs) {
    if (n[0]=="$") collect=true;
  }
  if (collect) {
    res=tagCreate("<","",[]);
    for (var n in attrs.attrs) {
      if (n[0]=="$") res.attrs[n]=attrs.attrs[n];
    }
    for (var n in node.attrs) {
      if (n[0]=="$") res.attrs[n]=node.attrs[n];
    }
  }
  return res;
}
function foreach_inheritAttrs(node,attrs) {
  if (!isDictionary(attrs) || !isDictionary(node)) error("foreach_inheritAttrs");
  for (var n in attrs.attrs) {
    if (n[0]=="$" && node.attrs[n]==undefined) node.attrs[n]=attrs.attrs[n];
  }
}
function inherit_attrs(node,attrs) {
  if (isDictionary(node)) {
    var ntag=node.tag;
    if (ntag!="!--") {
      foreach_inheritAttrs(node,attrs);
      attrs2=foreach_collectAttrs(attrs,node);
      var elt=node.elts;
      for (var i=0;i<elt.length;i++) {
        if (!isString(elt[i])) {
          inherit_attrs(elt[i],attrs2);
        }
      }
    }
  }
}

/*
 * dom.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// Expanding highlighted tags (DOM)
function dom_replace(tag,node,markup) {
  //if (cdata[tag]) {
    var div=document.createElement("div");
    div.innerHTML=markup;
    node.parentNode.replaceChild(div,node);
  /*}
    else {
      node.innerHTML=markup;
  }*/
}
function dom_transform(tag,transform) {
  var todo0=document.getElementsByTagName(tag),val=null;
  var todo=[];
  var i;
  for (i=0;i<todo0.length;i++) todo.push(todo0[i]);
  for (i=0;i<todo.length;i++) {
    var attrs={};
    for (var attr,j=0,a=todo[i].attributes,n=a.length;j<n;j++) {
      attr=a[j];
      attrs[attr.nodeName]=attr.value;
    }
    var text=transform(attrs,todo[i].innerHTML);
    if (text!=null) {
      dom_replace(tag,todo[i],text);
    }
  }
}

/*
 * hilite_cli.js
 *
 * Copyright (C) 2016, 2017  Sebastian Heimpel and Henri Lesourd
 *
 *  This file is part of hilite.js.
 *
 *  hilite.js is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  hilite.js is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with hilite.js.  If not, see <http://www.gnu.org/licenses/>.
 */

// TODO: Add options for controlling the indent level, adding line numbers, etc.
// Hilite script
function hilite_scriptRaw(lang,style,text) {
  var res="";
  if (lang=="") lang="html";
  enterScriptPretty(lang,style);
  if (isString(text)) {
    hilite_init();
    text=trim(text,"\n",1,0);
    text=trim(text," \n",0,1);
    bufstart(text);
    if (lang=="html") res=htmlPretty(htmlParse());
                 else res=scriptPretty(tokenize());
  }
  else {
    text.elts=arrayTrim(text.elts,true,true);
    if (lang=="html") res=htmlPretty(text);
                 else res=scriptPretty(text.elts);
  }
  leaveScriptPretty();
  return res;
}
function hilite_scriptCooked(lang,style,text) {
  if (isString(lang) && startsWith(lang,"inactive")) { // Inactive tags: we replace
    lang=lang.substring(9,lang.length);
    return "<code><table><tr><td class=\"hilite\">"+hilite_scriptRaw(lang,style,text)+
           "</td></tr></table></code>";
  }
  else return "";
}
function hilite_script(attrs,text) {
  return hilite_scriptCooked(attrVal(attrs["language"]),"code",text);
}

// Hilite hilite
var hilite_plaintextAttrs=transformCreate(),
    hilite_plaintextMarkup=transformCreate();
function attrLang(lang) {
  lang=attrVal(lang);
  lang=trim(replaceAll(lang,"vars","")," ",true,true);
  lang=trim(replaceAll(lang,"predefs","")," ",true,true);
  lang=trim(replaceAll(lang,"names","")," ",true,true);
  lang=trim(replaceAll(lang,"inactive","")," ",true,true);
  return lang;
}
function blockCla(tag,cla) {
  if (cla==null) cla="";
  cla=attrVal(cla).split(" ");
  var i=index(cla,"inline");
  if (i!=-1) cla[i]="Inline";
  else {
    i=index(cla,"block");
    if (i!=-1) cla[i]="Block";
    else cla.push(tag=="code"?"Block":"Inline");
  }
  return "\""+cla.join(" ")+"\"";
}
function plainLang(node,lang) {
  var tag=node.tag;
  node.tag="span";
  var node2=tagCreate("<","",[]);
  node2.elts=node.elts;
  var html=hilite_scriptRaw(lang,"plain",htmlSerialize(node2));
  node.elts=[html];
  var cla=node.attrs["class"];
  node.attrs["class"]=blockCla(tag,cla);
  return node;
}
function hilite_plaintextInit() {
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    var lang=attrVal(node.attrs["language"]);
    if (lang!="") node.attrs["$hilang"]='"'+lang+'"';
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="var" || node.tag=="vars") {
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=false;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="predef" || node.tag=="predefs") {
      node.attrs["$hivars"]=false;
      node.attrs["$hipredefs"]=true;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="name" || node.tag=="names") {
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=true;
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="ltt") { // FIXME: Why can't <ltt>...</ltt> be in hilite_plaintextMarkup ?
      node.attrs["$hivars"]=true;
      node.attrs["$hipredefs"]=false;
      var lang=attrLang(node.attrs["$hilang"]);
      foreach_text(node,function(node,attrs) {
        var res=node,tt=false;
        if (node!="," && node!=";" && !charIs(node,BLANK)) tt=true;
        if (tt) {
          var s=res;
          res=tagCreate("<>","tt",[]);
          if (lang!="") enterScriptPretty(lang,"plain");
          res.attrs["class"]="\""+scriptStyle+" ListOfTT\"";
          if (lang!="") leaveScriptPretty();
          res.attrs["$hivars"]=true; // TODO: Check if this is necessary
          res.elts.push(s);
        }
        return res;
      });
    }
    return node;
  });
  transformAddRule(hilite_plaintextAttrs,function(node,attrs) {
    if (node.tag=="script" || node.tag=="code" || node.tag=="c"
     || contains(node.tag,"htm") || contains(node.tag,"css")
     || contains(node.tag,"js") || contains(node.tag,"php") || contains(node.tag,"sql")) // FIXME: Unify the place where all these language names are defined ...
    {
      node.attrs["$hivars"]=false;
      node.attrs["$hipredefs"]=false;
    }
    return node;
  });
  
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    var tag=node.tag;
    if (tag=="c" || tag=="code") {
      var lang=attrLang(node.attrs["$hilang"]);
      node=plainLang(node,lang);
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="htm" || node.tag=="htm+code" || node.tag=="code+htm") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"html");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="css" || node.tag=="css+code" || node.tag=="code+css") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"css");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="js" || node.tag=="js+code" || node.tag=="code+js") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"javascript");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="php" || node.tag=="php+code" || node.tag=="code+php") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"php");
    }
    return node;
  });
  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="sql" || node.tag=="sql+code" || node.tag=="code+sql") {
      if (contains(node.tag,"code")) node.tag="code";
      node=plainLang(node,"sql");
    }
    return node;
  });

  transformAddRule(hilite_plaintextMarkup,function(node,attrs) {
    if (node.tag=="script") {
      node.attrs["foldtag"]=1;
      node.elts=arrayTrim(node.elts,true,true);
      node=tagCreate("<>","td",[node]);
      node.attrs["class"]="\"hilite\"";
      node=tagCreate("<>","tr",[node]);
      node=tagCreate("<>","table",[node]);
      node=tagCreate("<>","code",[node]);
      foreach_cut();
    }
    return node;
  });
}

function hilite_plaintextNames(node,attrs) {
  foreach_text(node,function(node,attrs) {
    var res=node;
    if (length(node)>1 && node[0]=="\\" && node[1]!="\\") res=node.substring(1,node.length);
    res=scriptReplaceEscapedSymbs(res,false);
    if (isScriptVar(node) && attrs.attrs["$hivars"]) res=scriptPrettyVar(node);
    else
    if (isScriptPred(node) && attrs.attrs["$hipredefs"]) res=scriptPrettyPredef(node);
    return res;
  });
  return node;
}
var hilite_prewrap=false;
function hilite_hilite(attrs,text) {
//alert(display(text));
  var lang=attrVal(attrs["language"]),res;
  if (isString(lang) && startsWith(lang,"inactive")) res=hilite_script(attrs,text);
  else {
    var vars=true,predefs=false;
    if (lang==undefined || lang==null || lang=="") lang="html";
    else {
      if (contains(lang,"vars")) lang=trim(replaceAll(lang,"vars","")," ",true,true),vars=true;
                                                                                else vars=false;
      if (contains(lang,"predefs")) lang=trim(replaceAll(lang,"predefs","")," ",true,true),predefs=true;
      if (contains(lang,"names")) lang=trim(replaceAll(lang,"names","")," ",true,true),vars=predefs=true;
    }
    if (!scriptLangExists(lang)) error("Unknown language: "+lang);
    var tree;
    if (isString(text)) {
      hilite_init();
      bufstart(text);
      tree=htmlParse();
    }
    else tree=tagCreate(text.state,"",text.elts);
    tree.attrs["$hilang"]=lang;
    inherit_attrs(tree,tree);
    transform(tree,hilite_plaintextAttrs);
    tree.attrs["$hivars"]=vars;
    tree.attrs["$hipredefs"]=predefs;
    inherit_attrs(tree,tree);
    enterScriptPretty(lang,"plain");
    hilite_plaintextNames(tree,tree);
    leaveScriptPretty();
  //alert(display(tree));
    transform(tree,hilite_plaintextMarkup);
    if (hilite_prewrap) tree.elts=arrayTrim(tree.elts,true,true);
    res=htmlSerializePlainText(tree,hilite_prewrap); // TODO: default language should be HTML here (?)
  }
  return res;
}

// Init
function hiliteInit(prewrap) {
  hilite_plaintextInit();
  hilite_prewrap=prewrap;
  dom_transform("hilite",hilite_hilite);
  dom_transform("script",hilite_script);
  window.document.body.style.display="block";
}


var src,build;

// Build
function build() {
  var srcd=dirRead(src);

  var jslang=null,langnames=[],aliases=[],sensitivity={ keyw:{},pred:{} };
  foreach_vfile(srcd,function (f) { // Expanding the .lang files
    if (!f.isDir && endsWith(f.fname,".lang")) {
      if (jslang==null) jslang="";
      var fname=vfilePathname(f);
      var dir=path.dirname(fname)+"/";
      fname=path.basename(fname);
    //console.log(dir+";;"+fname);
      fname=fname.substring(0,fname.length-5);
      var js="";
      var gen=function(name,tab) {
        var res="var "+name+"={";
        for (var i=0;i<tab.length;i++) {
          res+=" \""+tab[i]+"\":1";
          if (i+1<tab.length) res+=",";
        }
        res+=" };";
        return res;
      };
      if (fname=="html") {
        var line=f.val.split("\n");
        var do_selftag=false,selftag="",
            do_optclose=false,optclose="",
            do_cdata=false,cdata="";
        var reset=function() { do_selftag=do_optclose=do_cdata=false; };
        for (var i=0;i<line.length;i++) {
          if (length(line[i])>1 && line[i][0]=="#") continue;
          var s=trim(line[i]," ",1,1);
          if (s=="selftag") reset(),do_selftag=true;
          else
          if (s=="optclose") reset(),do_optclose=true;
          else
          if (s=="cdata") reset(),do_cdata=true;
          else {
            if (do_selftag) selftag+=s+" ";
            if (do_optclose) optclose+=s+" ";
            if (do_cdata) cdata+=s+" ";
          }
        }
        selftag=trim(selftag," ",1,1).split(" ");
        optclose=trim(optclose," ",1,1).split(" ");
        cdata=trim(cdata," ",1,1).split(" ");
        js+="// html\n";
        js+=gen("selftag",selftag)+"\n";
        js+=gen("optclose",optclose)+"\n";
        js+=gen("cdata",cdata)+"\n";
        langnames.push("html");
      }
      else {
        var line=f.val.split("\n");
        var do_op=false,op="",
            do_keyw=false,keyw="",
            do_pred=false,pred="",
            keyw_sensitive=true,
            pred_sensitive=true;
        var reset=function() { do_op=do_keyw=do_pred=false; };
        for (var i=0;i<line.length;i++) {
          if (length(line[i])>1 && line[i][0]=="#") continue;
          var s=trim(line[i]," ",1,1);
          if (startsWith(s,"alias ")) {
            reset();
            s=trim(s.substring(6,s.length)," ",true,true);
            aliases.push([s,fname]);
          }
          if (s=="op") reset(),do_op=true;
          else
          if (s=="keyw" || startsWith(s,"keyw ")) {
            if (startsWith(s,"keyw ")) {
              s=trim(s.substring(5,s.length)," ",true,true);
              if (s=="sensitive") keyw_sensitive=true;
              if (s=="insensitive") keyw_sensitive=false;
            }
            reset(),do_keyw=true;
          }
          else
          if (s=="pred" || startsWith(s,"pred ")) {
            if (startsWith(s,"pred ")) {
              s=trim(s.substring(5,s.length)," ",true,true);
              if (s=="sensitive") pred_sensitive=true;
              if (s=="insensitive") pred_sensitive=false;
            }
            reset(),do_pred=true;
          }
          else {
            if (do_op) op+=s+" ";
            if (do_keyw) {
              if (!keyw_sensitive) s=s.toLowerCase();
              keyw+=s+" ";
            }
            if (do_pred) {
              if (!pred_sensitive) s=s.toLowerCase();
              pred+=s+" ";
            }
          }
        }
        op=trim(op," ",1,1).split(" ");
        keyw=trim(keyw," ",1,1).split(" ");
        pred=trim(pred," ",1,1).split(" ");
        js+="// "+fname+"\n";
        js+=gen(fname+"op",op)+"\n";
        js+=gen(fname+"keyw",keyw)+"\n";
        js+=gen(fname+"pred",pred)+"\n";
        langnames.push(fname);
        sensitivity.keyw[fname]=keyw_sensitive;
        sensitivity.pred[fname]=pred_sensitive;
      }
      jslang+=js+"\n";
    }
  });
  if (jslang!=null) {
    jslang+="// lang\nvar lang={\n";
    jslang+="  \"html\":{ \"style\": { \"code\":\"html\", \"plain\":\"phtml\" } },\n";
    for (var i=0;i<langnames.length;i++) {
      var lang=langnames[i];
      if (lang!="html") {
        jslang+="  \""+lang+"\":{ \"style\":\n    { \"code\":\""+lang+"\", \"plain\":\"p"+lang+"\" },\n";
        jslang+="    \"op\":"+lang+"op, \"keyw\":"+lang+"keyw, \"pred\":"+lang+"pred, \"keyw_sensitive\":"+sensitivity.keyw[lang]+", \"pred_sensitive\":"+sensitivity.pred[lang]+" }";
        if (i+1<langnames.length) jslang+=",";
        jslang+="\n";
      }
    }
    jslang="// This file has been generated from the .lang files in your src directory. Don't modify it directly.\n"+jslang;
    jslang+="};\n"
    for (var i=0;i<aliases.length;i++) {
      jslang+="lang[\""+aliases[i][0]+"\"]=lang[\""+aliases[i][1]+"\"];\n";
    }
    fileWrite(src+"/langs.js",jslang);
  }

  foreach_vfile(srcd,function (f) { // Expanding the .prj files
    if (!f.isDir && endsWith(f.fname,".prj")) {
      var fname=vfilePathname(f);
      var dir=path.dirname(fname)+"/";
      fname=path.basename(fname);
      fname=fname.substring(0,fname.length-3)+"js";
    //console.log(dir+";;"+fname);
      var line=f.val.split("\n");
      var js="";
      for (var i=0;i<line.length;i++) {
        var incl=trim(line[i]," ",1,1);
        if (startsWith(incl,"include")) {
          incl=incl.substring(7,incl.length);
          incl=trim(incl," ;",1,1);
          incl=trim(incl,"\"",1,1);
          js+=fileRead(dir+incl,"ascii")+"\n\n";
        //console.log(incl);
        }
        else {
          js+=line[i];
          if (i<line.length) js+="\n";
        }
      }
      console.log("Writing "+build+"/"+fname);
      fileWrite(build+"/"+fname,js);
    }
  });
}

// Validate
function validate() {
  var srcd=dirRead(src);
  hilite_plaintextInit();

  foreach_vfile(srcd,function (f) { // Validating the .html files
    if (!f.isDir && (endsWith(f.fname,".txt") || endsWith(f.fname,".html"))) { // TODO: Remove validating .txt files as HTML
      console.log("Validating "+f.fname);
      foreach_elt(f.val,function(node) {
        if (node.tag=="hilite") {
          node.elts=[hilite_hilite(node.attrs,tagCreate("<>","",node.elts))];
          node.tag="div";
          node.attrs={};
        }
        else
        if (node.tag=="script") {
          node.elts=[hilite_script(node.attrs,tagCreate("<>","",node.elts))];
          node.tag="span";
          node.attrs={};
        }
        return node;
      });
    //console.log(htmlSerialize(f.val));
    }
  });
}

// Main
function main() {
  SERVER=true;

  var operation=build;
  if (process.argv[2]=="-build" || process.argv[2]=="-validate") {
    if (process.argv[2]=="-validate") operation=validate;
    argvRemove(process.argv,2);
  }
  if (length(process.argv)<3) src=".";
                         else src=trim(process.argv[2],"/",false,true);
  if (length(process.argv)<4) build=".";
                         else build=trim(process.argv[3],"/",false,true);

/*console.log("src="+src);
  console.log("build="+build);
  console.log("---");*/
  operation();
}
main();
